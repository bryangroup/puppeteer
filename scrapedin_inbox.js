// node scrapedin_inbox.js --messages= {"1":{"messages":[{"id":15,"id_interlocutor":573914,"url":"https:\/\/www.linkedin.com\/in\/simone-penza\/","ext":1},{"id":14,"id_interlocutor":573914,"url":"https:\/\/www.linkedin.com\/in\/simone-penza\/","ext":1}],"cookie":"AQEDAQEjVDYDBx63AAABgUH44UcAAAGBZgVlR00AUb2sL9diQX4q0L_bP4G7seiWYaZkewm8L7tO09kk5ObyWqxkApLuNeQ0fqfryZguLhamu2licGnYJsCMh-ZpEkgNnymCQ5IAeTKJCrTbZyzExn2w"}}


const puppeteer = require('puppeteer');
const fs = require('fs');
const util = require('util');

function getArgs () {
    const args = {};
    process.argv
        .slice(2, process.argv.length)
        .forEach( arg => {
            // long arg
            if (arg.slice(0,2) === '--') {
                const longArg = arg.split('=');
                const longArgFlag = longArg[0].slice(2,longArg[0].length);
                const longArgValue = longArg.length > 1 ? longArg[1] : true;
                args[longArgFlag] = longArgValue;
            }
            // flags
            else if (arg[0] === '-') {
                const flags = arg.slice(1,arg.length).split('');
                flags.forEach(flag => {
                    args[flag] = true;
                });
            }
        });
    return args;
}

(async () => {

    var is_debug = false;
    var jquery_path = 'node_modules/jquery/dist/jquery.js'

    puppeteer_options = {};

    if(__dirname.startsWith('/Users/')) {
        is_debug = true;
        console.log('ScrapedIn running locally - automatic "is_debug" true');
    }
    puppeteer_options = {
        ignoreHTTPSErrors: true,
        headless: false,
        devtools: true,
        args: ["--no-sandbox", '--disable-web-security']
    };

    var jquery_path = 'node_modules/jquery/dist/jquery.js'
    if(!is_debug) {
        puppeteer_options = {
            ignoreHTTPSErrors: true,
            headless: true,
            devtools: false,
            defaultViewport: { width: 1600, height: 800 },
            args: ["--no-sandbox",  '--disable-web-security']
        };
        jquery_path = '/var/www/puppeteer/node_modules/jquery/dist/jquery.js'
    }

    const args = getArgs();

    var messages = args.messages;
    var authcookie = '';


    messages = JSON.parse(messages);

    for(var key in messages){
        
        var user_messages = messages[key];

        console.log(messages[key]['cookie'])

        const browser = await puppeteer.launch(puppeteer_options);

        const page = await browser.newPage();

        authcookie = user_messages['cookie'];

        // console.log(user_messages)

        // console.log(authcookie)
        // console.log('********');

        const cookie = {
            name: 'li_at',
            value: authcookie,
            domain: '.www.linkedin.com',
            url: 'https://www.linkedin.com',
            path: '/',
            httpOnly: true,
            secure: true
        }

        console.log(cookie)

        try {
            await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36");
            await page.setCookie(cookie)
            await page.setBypassCSP(true);

        } catch(err) {
            console.log(err);
        }

        for( var key2 in user_messages['messages'] ){

            var user_mex = user_messages['messages'][key2];

            console.log(user_mex.url)

            var id_user = user_mex.ext;

            // console.log('-------');

            console.log('Opening page ...');
            try {
                await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36");
                await page.setCookie(cookie)
                await page.setBypassCSP(true);
                await page.goto(user_mex.url, {timeout: 180000});
                async function injectFile(page, filePath) {
                    let contents = await new Promise((resolve, reject) => {
                        fs.readFile(filePath, 'utf8', (err, data) => {
                            if (err) return reject(err);
                            resolve(data);
                        });
                    });
                    contents += `//# sourceURL=` + filePath.replace(/\n/g,'');
                    return page.mainFrame().evaluate(contents);
                }
                await injectFile(page, jquery_path);

                console.log('qui')


                await page.waitForTimeout(4000)

                const [chat_button] = await page.$$(".artdeco-card .pvs-profile-actions a.artdeco-button[href*='/messaging/thread/']");
                await chat_button.click();
                
                const page_result = await page.evaluate(({id_user}) => {

                    function launch_main_interval(){
                        return new Promise( function(resolve){
                            const $ = window.$;
                            const ajax_url = 'https://commercharlie.com/scrappup/request';

                            var chat = [];

                            setTimeout(function(){

                                var count_main_conv = 0;

                                

                                    count_main_conv += 10000;

                                    var
                                    count_conv = 0,
                                    convs = {};

                                    setTimeout(function(){

                                        var
                                        avatar,
                                        name,
                                        conv_user,
                                        conv_hour,
                                        conv_text;

                                        avatar = $('img.org-top-card-primary-content__logo').attr('src');
                                        name = $('.pv-text-details__left-panel h1').text();

                                        console.log(name)

                                        convs.name = name;
                                        convs.avatar = avatar;
                                        convs.conv = [];

                                        count_conv += 1000;

                                        setTimeout(function(){

                                            if( $('.msg-s-message-list-content .msg-s-message-list__event').length > 0 ){

                                                $('.msg-s-message-list-content .msg-s-message-list__event').each(function(){

                                                    var $this = $(this);

                                                    conv_user = $this.find('.msg-s-message-group__meta .msg-s-message-group__profile-link .msg-s-message-group__name').text();
                                                    conv_hour = $this.find('.msg-s-message-group__meta time').text();
                                                    conv_text = $this.find('.msg-s-event__content').html();

                                                    console.log(conv_user)
                                                    console.log(conv_hour)
                                                    console.log(conv_text)

                                                    if( conv_user == name){
                                                        convs.conv.push({
                                                            'user' : conv_user,
                                                            'date' : conv_hour,
                                                            'text' : conv_text
                                                        });
                                                    } else{
                                                        convs.conv.push({
                                                            'user' : conv_user,
                                                            'date' : conv_hour,
                                                            'text' : conv_text,
                                                            'type' : sent
                                                        });
                                                    }

                                                })

                                            }

                                        }, count_conv);

                                        chat.push(convs)

                                        console.log(chat)

                                        console.log(chat.length)

                                        // if( chat.length == 4 ){
                                        //     console.log('ajax start')
                                        //     var request_data = {
                                        //         'action': 'inbox_update',
                                        //         'id_user': id_user,
                                        //         'chat' : removeExtraSpaces(JSON.stringify(chat))
                                        //     };

                                        //     $.ajax({
                                        //         type: "POST",
                                        //         url: ajax_url,
                                        //         dataType: "JSON",
                                        //         async: true,
                                        //         data: request_data,
                                        //         success: function (resp) {
                                        //             console.log('ajax success')
                                        //             resolve(chat);
                                        //         },
                                        //         error: function (request, status, error) {
                                        //             console.log('ajax error')  
                                        //             resolve(error);
                                        //         }
                                        //     });
                                        // }


                                    }, count_main_conv);

                                

                            }, 1000);
                        })

                        function removeExtraSpaces(string) {
                            string = string.replace(/\s{2,}/g, '');
                            return string.replace('/\n', '');
                        }

                    }

                    return launch_main_interval().then(function(x){ return x; });

                }, {id_user});
                

                console.log(page_result);

            } catch(err) {
                console.log(err);
            }

        }

      }

      await browser.close();

})();
