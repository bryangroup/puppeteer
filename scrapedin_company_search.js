const puppeteer = require('puppeteer');
const fs = require('fs');
const util = require('util');
const log_file = fs.createWriteStream(__dirname + '/storage/logs/scrapedin_company_search.log', {flags: 'w'});

function getArgs () {
    const args = {};
    process.argv
        .slice(2, process.argv.length)
        .forEach( arg => {
            // long arg
            if (arg.slice(0,2) === '--') {
                const longArg = arg.split('=');
                const longArgFlag = longArg[0].slice(2,longArg[0].length);
                const longArgValue = longArg.length > 1 ? longArg[1] : true;
                args[longArgFlag] = longArgValue;
            }
            // flags
            else if (arg[0] === '-') {
                const flags = arg.slice(1,arg.length).split('');
                flags.forEach(flag => {
                    args[flag] = true;
                });
            }
        });
    return args;
}

(async () => {
    var is_debug = false;
    if(__dirname.startsWith('/Users/')) {
        is_debug = true;
        console.log('ScrapedIn running locally - automatic "is_debug" true');
    }
    var puppeteer_options = {
        ignoreHTTPSErrors: true,
        headless: false,
        devtools: true,
        args: ["--no-sandbox",  '--disable-web-security']
    };
    var jquery_path = 'node_modules/jquery/dist/jquery.js'
    if(!is_debug) {
        puppeteer_options = {
            ignoreHTTPSErrors: true,
            headless: true,
            devtools: false,
            args: ["--no-sandbox",  '--disable-web-security']
        };
        jquery_path = '/var/www/puppeteer/node_modules/jquery/dist/jquery.js'
    }

    Date.prototype.addHours= function(h){
        this.setHours(this.getHours()+h);
        return this;
    }
    Date.prototype.addMinutes= function(m){
        this.setMinutes(this.getMinutes()+m);
        return this;
    }

    var end_time = new Date().addMinutes(30);
    const args = getArgs();
    var process_id = process.pid;
    var authcookie = args.cookie;
    console.log('launched');
    var ext = args.ext;

    const browser = await puppeteer.launch( puppeteer_options );

    const cookie = {
        name: 'li_at',
        value: authcookie,
        domain: '.www.linkedin.com',
        url: 'https://www.linkedin.com',
        path: '/',
        httpOnly: true,
        secure: true
    }
    var page = await browser.newPage();
    var pup_page = 'https://www.linkedin.com';
    var page_result = '';

    // function node_logger(id_user, id_process, current_page, text, is_error = false) {
    //     const date_ob = new Date();
    //     const hours = date_ob.getHours();
    //     const minutes = date_ob.getMinutes();
    //     const seconds = date_ob.getSeconds();
    //     const day = date_ob.getDate();
    //     const month = date_ob.getMonth();
    //     const year = date_ob.getFullYear();
    //     const dt = '['+year+'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds+']';
    //     const log_line = dt + ' idu ' + id_user + ' - idp ' + id_process + ' - url ' + current_page + ': ' + util.format(text) + ' \n';
    //     log_file.write( log_line );
    // }

    try {
        async function injectFile(page, filePath) {
            let contents = await new Promise((resolve, reject) => {
                fs.readFile(filePath, 'utf8', (err, data) => {
                    if (err) return reject(err);
                    resolve(data);
                });
            });
            contents += `//# sourceURL=` + filePath.replace(/\n/g, '');
            return page.mainFrame().evaluate(contents);
        }

        while(!page_result.includes('stop ')) {
            const waitTillHTMLRendered = async (page, timeout = 30000) => {
                const checkDurationMsecs = 1000;
                const maxChecks = timeout / checkDurationMsecs;
                let lastHTMLSize = 0;
                let checkCounts = 1;
                let countStableSizeIterations = 0;
                const minStableSizeIterations = 3;

                while(checkCounts++ <= maxChecks){
                    let html = await page.content();
                    let currentHTMLSize = html.length;

                    // let bodyHTMLSize = await page.evaluate(() => document.body.innerHTML.length);
                    // console.log('last: ', lastHTMLSize, ' <> curr: ', currentHTMLSize, " body html size: ", bodyHTMLSize);

                    if(lastHTMLSize != 0 && currentHTMLSize == lastHTMLSize)
                        countStableSizeIterations++;
                    else
                        countStableSizeIterations = 0; //reset the counter

                    if(countStableSizeIterations >= minStableSizeIterations) {
                        // console.log("Page rendered fully..");
                        break;
                    }

                    lastHTMLSize = currentHTMLSize;
                    await page.waitForTimeout(checkDurationMsecs);
                }
            };
            // node_logger(ext, process_id, pup_page, 'Launched');
            await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36");
            await page.setCookie(cookie)
            await page.setBypassCSP(true);
            await page.goto(pup_page, {waitUntil: "load", timeout: 180000});
            await waitTillHTMLRendered(page)
            await injectFile(page, jquery_path);
            // node_logger(ext, process_id, pup_page, 'jQuery injected');

            page_result = await page.evaluate(({ext, authcookie, end_time, process_id}) => {
                /** FUNCTIONS - START **/
                function getUrlParameter(sParam) {
                    let sPageURL = window.location.search.substring(1),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');

                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                        }
                    }
                }

                function removeLastWord(str) {
                    const lastIndexOfSpace = str.lastIndexOf(' ');
                    return str.substring(0, lastIndexOfSpace);
                }

                // function logger(id_user, id_process, current_page, text, is_error = false){
                //     const log_line = 'idu ' + id_user + ' - idp ' + id_process + ' - url ' + current_page + ': ' + JSON.stringify(text) + ' \n';
                //     const ajax_data = {
                //         'action': 'scrap_log',
                //         'text': log_line,
                //         'is_error': is_error,
                //     };
                //
                //     $.ajax({
                //         type: "POST",
                //         url: 'https://scrap.commercharlie.com/scrapedin/request',
                //         dataType: "JSON",
                //         async: false,
                //         data: ajax_data,
                //         success: function (resp) {
                //         }
                //     });
                // }
                function removeExtraSpaces(string) {
                    return string.replace(/\s{2,}/g, '');
                }

                /** FUNCTIONS - END **/

                var $ = window.$;
                const ajax_url = 'https://commercharlie.com/scrappup/request';

                function launch_main_interval() {
                    return new Promise(function (resolve) {
                        var cookie_interval = setInterval(function () {
                            let is_cookie_ok = $('html.theme').length;
                            var cookie_status = 'expired';
                            var current_url = window.location.href;

                            if(is_cookie_ok <= 0){
                                // Check Sales navigator
                                if($('.login__form').length <= 0 && $('.enterprise-application-header'))
                                    is_cookie_ok = 1;
                            } else {
                                // Check Login form with html.theme = 1
                                if(
                                    $('.login__form').length >= 1 ||
                                    $('.sign-in-form-container').length >= 1
                                ){
                                    is_cookie_ok = 0;
                                }
                            }

                            if(current_url == 'https://www.linkedin.com/hp')
                                is_cookie_ok = 0;

                            if (is_cookie_ok > 0)
                                cookie_status = 'ok';

                            console.log('Online: ' + is_cookie_ok);
                            console.log($('html'));
                            console.log(current_url);
                            // console.log('Login: ' + $('.login__form').length);

                            Date.prototype.addHours= function(h){
                                this.setHours(this.getHours()+h);
                                return this;
                            }
                            var date_now_ob = new Date().addHours(0);
                            var end_date_ob = new Date(end_time);

                            console.log(date_now_ob);
                            console.log(end_date_ob);

                            // if (date_now_ob >= end_date_ob){
                            //     resolve('stop 30minutes');
                            // }

                            var request_data = {
                                'action': 'company_search_cookie_status',
                                'id_user': ext,
                                'cookie_val': authcookie,
                                'cookie_status': cookie_status,
                                'id_process': process_id,
                            };

                            var expired = false;

                            $.ajax({
                                type: "POST",
                                url: ajax_url,
                                dataType: "JSON",
                                async: false,
                                data: request_data,
                                success: function (resp) {
                                    console.log('machine status: ' + resp);
                                    // // logger(ext, process_id, current_url,'company_search_cookie_status AJAX ok - status '+cookie_status, false);
                                },
                                error: function (request, status, error) {
                                    // logger(ext, process_id, current_url,'company_search_cookie_status AJAX ERROR - '+request.responseText, true);
                                    resolve('stop 1');
                                }
                            });

                            if(expired)
                                resolve('stop expired');

                            if (is_cookie_ok <= 0) {
                                resolve('stop 2');
                            }

                            var is_interrupted = false;
                            var change_page = false;
                            var is_company_search = false;
                            var company_urlencoded = '';
                            current_url = current_url.replaceAll(',', '%2C');
                            var search_input = $('.search-global-typeahead__input');

                            console.log('--- SCRAPER started ---');

                            var data_check_page = {
                                action: 'check_page_company_search',
                                client_id: ext,
                            };
                            var company_obj = '';

                            $.ajax({
                                type: "POST",
                                url: ajax_url,
                                dataType: "JSON",
                                async: false,
                                data: data_check_page,
                                success: function (resp) {
                                    company_obj = resp;
                                    if(company_obj === 'no'){
                                        // logger(ext, process_id, current_url,'check_page_company_search FINISH SCRAPING - ', false);
                                        resolve('stop finish 1');
                                    } else {
                                        company_urlencoded = encodeURIComponent(company_obj.company);
                                        // logger(ext, process_id, current_url,'check_page_company_search AJAX ok', false);
                                    }
                                },
                                error: function (request, status, error) {
                                    // logger(ext, process_id, current_url,'check_page_company_search AJAX ERROR - '+request.responseText, true);
                                    resolve('stop 4');
                                }
                            });

                            var check_double_page = 0;

                            if (current_url.includes('/signup/'))
                                is_interrupted = true;
                            if (current_url.includes('/authwall'))
                                is_interrupted = true;
                            if (current_url.includes('/challenge/'))
                                is_interrupted = true;
                            if (current_url.includes('/uas/login'))
                                is_interrupted = true;
                            if (current_url.includes('/in/unavailable') || current_url.includes('/404/') ) {
                                is_interrupted = true;
                            }
                            if(current_url.includes('search/results/companies')){
                                is_company_search = true;
                            }

                            if (is_interrupted) {
                                var data_interrupted = {
                                    action: 'interrupted_company_search',
                                    client_id: ext,
                                };

                                $.ajax({
                                    type: "POST",
                                    url: ajax_url,
                                    dataType: "JSON",
                                    async: false,
                                    data: data_interrupted,
                                    success: function (resp) {
                                        console.log(resp);
                                        // logger(ext, process_id, current_url,'interrupted_company_search AJAX ok', true);
                                        resolve('stop 5');
                                    },
                                    error: function (request, status, error) {
                                        // logger(ext, process_id, current_url,'interrupted_company_search AJAX ERROR - '+request.responseText, true);
                                        resolve('stop 6');
                                    }
                                });
                            }

                            if(!is_company_search)
                                change_page = true;

                            if(typeof company_obj.id === "undefined"){
                                resolve('stop 7');
                            }

                            company_urlencoded = company_urlencoded.replaceAll("'", '%27')

                            if(
                                (search_input.length && search_input.val() != company_obj.name) &&
                                !current_url.includes(company_urlencoded)
                            ){
                                change_page = true;
                            }

                            if(change_page){
                                var return_url = 'https://www.linkedin.com/search/results/companies/?companyHqGeo='+company_obj.geo;
                                if(company_obj.sectors != '')
                                    return_url += '&industry='+company_obj.sectors;
                                return_url += '&keywords='+company_urlencoded+'&origin=FACETED_SEARCH';
                                is_company_search = false;
                                resolve(return_url);
                            }

                            if(!is_interrupted && is_company_search){
                                const check_no_results_by_artdeco = $('.artdeco-empty-state').length;
                                const check_no_results_by_list = $('.search-results-container ul').length;

                                console.log(check_no_results_by_artdeco);
                                console.log(check_no_results_by_list);

                                if(check_no_results_by_artdeco >= 1 || check_no_results_by_list <= 0){
                                    // NO RESULTS
                                    // RETRY (rimosso il no results, rimuove 1 parola alla volta, se vuoto riprova senza settori, se fallisce riprova con google e da error da li)
                                    var data_retry = {
                                        action: 'company_search_retry',
                                        id_company: company_obj.id,
                                        client_id: ext,
                                    };

                                    $.ajax({
                                        type: "POST",
                                        url: ajax_url,
                                        dataType: "JSON",
                                        async: false,
                                        data: data_retry,
                                        success: function (resp) {
                                            if (resp !== 'no') {
                                                resolve(resp);
                                            } else
                                                resolve('stop finish 2');
                                        },
                                        error: function (request, status, error) {
                                            // logger(ext, process_id, current_url,'company_search_no_results AJAX ERROR - '+request.responseText, true);
                                            resolve('stop 8');
                                        }
                                    });
                                } else {
                                    // RESULTS
                                    const company_url = $('.search-results-container ul li:first-child a.app-aware-link').attr('href');
                                    var data_company_result = {
                                        action: 'company_search_add',
                                        id_company: company_obj.id,
                                        company_url: company_url,
                                        client_id: ext,
                                    };

                                    console.log('company results ok');
                                    console.log(data_company_result);

                                    $.ajax({
                                        type: "POST",
                                        url: ajax_url,
                                        dataType: "JSON",
                                        async: false,
                                        data: data_company_result,
                                        success: function (resp) {
                                            if(resp !== 'no') {
                                                resolve(resp);
                                            } else
                                                resolve('stop finish 3');
                                        },
                                        error: function (request, status, error) {
                                            // logger(ext, process_id, current_url,'company_search_no_results AJAX ERROR - '+request.responseText, true);
                                            resolve('stop 9');
                                        }
                                    });
                                }
                            }

                        }, 3500);
                    });

                }

                return launch_main_interval().then(function (x) {
                    return x;
                });
            }, {ext, authcookie, end_time, process_id});

            // Comando ritornato dal promise resolve
            console.log(page_result);
            // node_logger(ext, process_id, pup_page, 'Finish with page result: ' + page_result);

            if(page_result.includes('stop ')){
                await browser.close();
            }
            if(page_result.includes('https://') || page_result.includes('http://')){
                // node_logger(ext, process_id, pup_page, 'Page changed from '+ pup_page + ' to ' + page_result);
                pup_page = page_result;
            }
        }
    } catch(err) {
        console.log(err);
        // node_logger(ext, process_id, pup_page, err.message, true);
        await browser.close();
    }
})();
