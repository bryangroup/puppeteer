<?php

namespace App\Http\Controllers;

use App\Mail\GenericEmail;
use Illuminate\Http\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Mail;

class ScrapedinController extends Controller
{
    public function postScrapedin(Request $request) {
        $response_array = [
            'exec' => 'no_action',
            'cmd' => '',
            'output' => '',
            'return_var' => '',
        ];
        $action = $request->input('action');

        if($action == 'launch'){
            $id_user = $request->input('id_user');
            $cookie = $request->input('cookie');
            $id_status = $request->input('id_status');
            $cmd = 'node /var/www/puppeteer/scrapedin.js --ext='.$id_user.' --ids='.$id_status.' --cookie='.$cookie.' > /dev/null 2>/dev/null &';
            exec( $cmd, $output, $return_var);
            $response_array = [
                'exec' => 'launched',
                'cmd' => $cmd,
            ];

            return response()->json( $response_array )->setStatusCode(200);
        }

        if($action === 'target'){
            $id_user = $request->input('id_user');
            $cookie = $request->input('cookie');
            $id_company_user = $request->input('id_company_user');
            $id_target = $request->input('id_target');
            $page = $request->input('page');
            $cmd = 'node /var/www/puppeteer/scrapedin_target.js --ext='.$id_user.' --cookie='.$cookie.' --target='.$id_target.' --idu='.$id_company_user.' --page='.$page;
            exec($cmd, $output, $return_var);
            $response_array = [
                'exec' => 'launched',
                'cmd' => $cmd,
                'output' => $output,
                'return_var' => $return_var,
            ];

            return response()->json( $response_array )->setStatusCode(200);
        }

        if($action === 'target_log'){
            $is_error = $request->input('is_error');
            $text = $request->input('text');
            $orderLog = new Logger('scrapedin_target');
            $orderLog->pushHandler(new StreamHandler(storage_path('logs/scrapedin_target.log')));
            $orderLog->info('Target Page Evaluation', [ $text ]);
            $response_array = [
                'exec' => 'target_log',
                'cmd' => '',
                'output' => '',
                'return_var' => '',
            ];

            if((int)$is_error === 1){
                // EMAIL
                $details = [
                    'subject' => 'Errore Target ScrapedIn',
                    'title' => 'Errore Target ScrapedIn',
                    'body' => $text,
                ];
                Mail::to('hosting@bryan.it')->send(new GenericEmail($details) );
            }
            return response()->json( $response_array )->setStatusCode(200);
        }

        if($action === 'get_inbox'){
            $messages = $request->input('messages');
            $cmd = 'node /var/www/puppeteer/scrapedin_inbox.js --messages='.$messages;
            exec($cmd, $output, $return_var);
            $response_array = [
                'exec' => 'launched',
                'cmd' => $cmd,
                'output' => $output,
                'return_var' => $return_var,
            ];

            return response()->json( $response_array )->setStatusCode(200);
        }

        if($action === 'send_queue_inbox'){
            $messages = $request->input('messages');
            $cmd = 'node /var/www/puppeteer/scrapedin_inbox_queue.js --messages='.$messages;
            exec($cmd, $output, $return_var);
            $response_array = [
                'exec' => 'launched',
                'cmd' => $cmd,
                'output' => $output,
                'return_var' => $return_var,
            ];

            return response()->json( $response_array )->setStatusCode(200);
        }

        if($action === 'send_disc_inbox'){
            $messages = $request->input('messages');
            $cmd = 'node /var/www/puppeteer/scrapedin_inbox_disc.js --messages='.$messages;
            exec($cmd, $output, $return_var);
            $response_array = [
                'exec' => 'launched',
                'cmd' => $cmd,
                'output' => $output,
                'return_var' => $return_var,
            ];

            return response()->json( $response_array )->setStatusCode(200);
        }

        if($action == 'company_search'){
            $cookie = $request->input('cookie');
            $id_user = $request->input('id_user');
            $cmd = 'node /var/www/puppeteer/scrapedin_company_search.js --ext='.$id_user.' --cookie='.$cookie;
//            exec( $cmd, $output, $return_var);
            exec( $cmd );
            $response_array = [
                'exec' => 'launched',
                'cmd' => $cmd,
            ];

            return response()->json( $response_array )->setStatusCode(200);
        }



        return response()->json( $response_array )->setStatusCode(200);
    }
}
