const puppeteer = require('puppeteer');
const fs = require('fs');

function getArgs () {
    const args = {};
    process.argv
        .slice(2, process.argv.length)
        .forEach( arg => {
        // long arg
        if (arg.slice(0,2) === '--') {
            const longArg = arg.split('=');
            const longArgFlag = longArg[0].slice(2,longArg[0].length);
            const longArgValue = longArg.length > 1 ? longArg[1] : true;
            args[longArgFlag] = longArgValue;
        }
        // flags
        else if (arg[0] === '-') {
            const flags = arg.slice(1,arg.length).split('');
            flags.forEach(flag => {
            args[flag] = true;
            });
        }
    });
    return args;
}

(async () => {
    var start_date_ob = new Date();
    var start_hours = start_date_ob.getHours();
    var start_minutes = start_date_ob.getMinutes();
    var start_seconds = start_date_ob.getSeconds();
    var end_hours = (start_hours + 1);
    var end_time = end_hours+':'+start_minutes+':'+start_seconds;
    const args = getArgs();
    var authcookie = args.cookie;
    console.log('launched');

    const browser = await puppeteer.launch({
        ignoreHTTPSErrors: true,
        headless: true,
        devtools: false,
        args: ["--no-sandbox"]
    });

    const cookie = {
        name: 'li_at',
        value: authcookie,
        domain: '.www.linkedin.com',
        url: 'https://www.linkedin.com',
        path: '/',
        httpOnly: true,
        secure: true
    }
    //
    const page = await browser.newPage();

    try {
        await page.setCookie(cookie)
        await page.setBypassCSP(true);
        await page.goto('https://www.linkedin.com', { waitUntil: "load", timeout: 180000});
        async function injectFile(page, filePath) {
            let contents = await new Promise((resolve, reject) => {
                fs.readFile(filePath, 'utf8', (err, data) => {
                    if (err) return reject(err);
                    resolve(data);
                });
            });
            contents += `//# sourceURL=` + filePath.replace(/\n/g,'');
            return page.mainFrame().evaluate(contents);
        }
        await injectFile(page, 'node_modules/jquery/dist/jquery.js');

        var ext = args.ext;
        const page_result = await page.evaluate(({ext, authcookie, end_time}) => {

            /** FUNCTIONS - START **/
            function getUrlParameter(sParam) {
                let sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
            }

            function removeExtraSpaces(string){ return string.replace(/\s{2,}/g, '');}
            /** FUNCTIONS - END **/

            const $ = window.$;
            const ajax_url = 'https://commercharlie.com/scrappup/request';
            const is_cookie_ok = $('html.theme').length;
            var cookie_status = 'expired';
            if(is_cookie_ok > 0)
                cookie_status = 'ok';
            console.log('Online: '+is_cookie_ok);

            setInterval(function(){

            },3500);

            var request_data = {
                'action': 'cookie_status',
                'id_user': ext,
                'cookie_val': authcookie,
                'cookie_status': cookie_status
            };
            $.ajax({
                type: "POST",
                url: ajax_url,
                dataType: "JSON",
                async: false,
                data: request_data,
                success: function (resp) {
                    console.log(resp);
                    return resp;
                },
            });

            if(is_cookie_ok <= 0)
                return 'close';

        }, {ext, authcookie, end_time});

        if(page_result === 'close')
            await browser.close();

        console.log(page_result);
        await browser.close();
    } catch(err) {
        console.log(err);
        await browser.close();
    }
})();
