<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/scrapedin/request', 'App\Http\Controllers\ScrapedinController@postScrapedin')->name('get_scrapedin')->middleware('cors');
Route::post('/scrapedin/request', 'App\Http\Controllers\ScrapedinController@postScrapedin')->name('post_scrapedin')->middleware('cors');
