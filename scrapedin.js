const puppeteer = require('puppeteer');
const fs = require('fs');
const util = require('util');
const log_file = fs.createWriteStream(__dirname + '/storage/logs/scrapedin.log', {flags: 'w'});

function getArgs () {
    const args = {};
    process.argv
        .slice(2, process.argv.length)
        .forEach( arg => {
            // long arg
            if (arg.slice(0,2) === '--') {
                const longArg = arg.split('=');
                const longArgFlag = longArg[0].slice(2,longArg[0].length);
                const longArgValue = longArg.length > 1 ? longArg[1] : true;
                args[longArgFlag] = longArgValue;
            }
            // flags
            else if (arg[0] === '-') {
                const flags = arg.slice(1,arg.length).split('');
                flags.forEach(flag => {
                    args[flag] = true;
                });
            }
        });
    return args;
}

(async () => {
    var is_debug = false;
    if(__dirname.startsWith('/Users/')) {
        is_debug = true;
        console.log('ScrapedIn running locally - automatic "is_debug" true');
    }
    var puppeteer_options = {
        ignoreHTTPSErrors: true,
        headless: false,
        devtools: true,
        defaultViewport: { width: 1600, height: 800 },
        args: ["--no-sandbox",  '--disable-web-security', '--start-maximized']
    };
    var jquery_path = 'node_modules/jquery/dist/jquery.js'
    if(!is_debug) {
        puppeteer_options = {
            ignoreHTTPSErrors: true,
            headless: true,
            devtools: false,
            defaultViewport: { width: 1600, height: 800 },
            args: ["--no-sandbox",  '--disable-web-security']
        };
        jquery_path = '/var/www/puppeteer/node_modules/jquery/dist/jquery.js'
    }

    Date.prototype.addHours= function(h){
        this.setHours(this.getHours()+h);
        return this;
    }

    var end_time = new Date().addHours(1);
    const args = getArgs();
    var authcookie = args.cookie;
    console.log('launched');
    var process_id = process.pid;
    var ext = args.ext;
    var id_status = args.ids;

    const browser = await puppeteer.launch( puppeteer_options );

    const cookie = {
        name: 'li_at',
        value: authcookie,
        domain: '.www.linkedin.com',
        url: 'https://www.linkedin.com',
        path: '/',
        httpOnly: true,
        secure: true
    }
    var page = await browser.newPage();
    var pup_page = 'https://www.linkedin.com';
    var page_result = '';

    function node_logger(id_user, id_process, current_page, text, is_error = false) {
        const date_ob = new Date();
        const hours = date_ob.getHours();
        const minutes = date_ob.getMinutes();
        const seconds = date_ob.getSeconds();
        const day = date_ob.getDate();
        const month = date_ob.getMonth();
        const year = date_ob.getFullYear();
        const dt = '['+year+'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds+']';
        const log_line = dt + ' idu ' + id_user + ' - idp ' + id_process + ' - url ' + current_page + ': ' + util.format(text) + ' \n';
        log_file.write( log_line );
    }

    try {
        async function injectFile(page, filePath) {
            let contents = await new Promise((resolve, reject) => {
                fs.readFile(filePath, 'utf8', (err, data) => {
                    if (err) return reject(err);
                    resolve(data);
                });
            });
            contents += `//# sourceURL=` + filePath.replace(/\n/g, '');
            return page.mainFrame().evaluate(contents);
        }

        while(!page_result.includes('stop ')) {
            const waitTillHTMLRendered = async (page, pup_page, timeout = 30000) => {
                const checkDurationMsecs = 1000;
                const maxChecks = timeout / checkDurationMsecs;
                let lastHTMLSize = 0;
                let checkCounts = 1;
                let countStableSizeIterations = 0;
                const minStableSizeIterations = 3;


                if(pup_page.includes('sales/search')) {
                    await page.waitForSelector('.enterprise-application-header');
                } else {
                    while(checkCounts++ <= maxChecks){
                        let html = await page.content();
                        let currentHTMLSize = html.length;
                        // let bodyHTMLSize = await page.evaluate(() => document.body.innerHTML.length);
                        // console.log('last: ', lastHTMLSize, ' <> curr: ', currentHTMLSize, " body html size: ", bodyHTMLSize);

                        if(lastHTMLSize != 0 && currentHTMLSize == lastHTMLSize)
                            countStableSizeIterations++;
                        else
                            countStableSizeIterations = 0; //reset the counter

                        if(countStableSizeIterations >= minStableSizeIterations) {
                            // console.log("Page rendered fully..");
                            break;
                        }

                        lastHTMLSize = currentHTMLSize;
                        await page.waitForTimeout(checkDurationMsecs);
                    }
                }
            };
            node_logger(ext, process_id, pup_page, 'Launched');
            await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36");
            await page.setCookie(cookie)
            await page.setBypassCSP(true);
            await page.goto(pup_page, {waitUntil: "load", timeout: 180000});
            await waitTillHTMLRendered(page, pup_page)
            await injectFile(page, jquery_path);
            node_logger(ext, process_id, pup_page, 'jQuery injected');

            page_result = await page.evaluate(({ext, authcookie, id_status, end_time, process_id}) => {
                /** FUNCTIONS - START **/
                function getUrlParameter(sParam) {
                    let sPageURL = window.location.search.substring(1),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');

                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                        }
                    }
                }
                function logger(id_user, id_process, current_page, text, is_error = false){
                    const log_line = 'idu ' + id_user + ' - idp ' + id_process + ' - url ' + current_page + ': ' + JSON.stringify(text) + ' \n';
                    const ajax_data = {
                        'action': 'scrap_log',
                        'text': log_line,
                        'is_error': is_error,
                    };

                    // $.ajax({
                    //     type: "POST",
                    //     url: 'https://scrap.commercharlie.com/scrapedin/request',
                    //     dataType: "JSON",
                    //     async: false,
                    //     data: ajax_data,
                    //     success: function (resp) {
                    //     }
                    // });
                }
                function removeExtraSpaces(string) {
                    return string.replace(/\s{2,}/g, '');
                }

                /** FUNCTIONS - END **/

                var $ = window.$;
                const ajax_url = 'https://commercharlie.com/scrappup/request';

                function launch_main_interval() {
                    return new Promise(function (resolve) {
                        var cookie_interval = setInterval(function () {
                            let is_cookie_ok = $('html.theme').length;
                            var cookie_status = 'expired';
                            var machine_status = 'launched';
                            var current_url = window.location.href;

                            if(is_cookie_ok <= 0){
                                // Check Sales navigator
                                if($('.login__form').length <= 0 && $('.enterprise-application-header'))
                                    is_cookie_ok = 1;
                            } else {
                                // Check Login form with html.theme = 1
                                if(
                                    $('.login__form').length >= 1 ||
                                    $('.sign-in-form-container').length >= 1
                                ){
                                    is_cookie_ok = 0;
                                }
                            }

                            if(current_url == 'https://www.linkedin.com/hp')
                                is_cookie_ok = 0;

                            if (is_cookie_ok > 0)
                                cookie_status = 'ok';

                            console.log('Online: ' + is_cookie_ok);
                            console.log($('html'));
                            console.log(current_url);
                            // console.log('Login: ' + $('.login__form').length);

                            Date.prototype.addHours= function(h){
                                this.setHours(this.getHours()+h);
                                return this;
                            }
                            var date_now_ob = new Date().addHours(0);
                            var end_date_ob = new Date(end_time);

                            console.log(date_now_ob);
                            console.log(end_date_ob);

                            if (date_now_ob >= end_date_ob){
                                cookie_status = 'relaunch';
                            }

                            var request_data = {
                                'action': 'cookie_status',
                                'id_user': ext,
                                'cookie_val': authcookie,
                                'cookie_status': cookie_status,
                                'id_status': id_status,
                                'id_process': process_id,
                            };

                            $.ajax({
                                type: "POST",
                                url: ajax_url,
                                dataType: "JSON",
                                async: false,
                                data: request_data,
                                success: function (resp) {
                                    console.log('machine status: ' + resp);
                                    machine_status = resp;
                                    logger(ext, process_id, current_url,'cookie_status AJAX ok - status '+cookie_status, true);
                                },
                                error: function (request, status, error) {
                                    logger(ext, process_id, current_url,'cookie_status AJAX ERROR - '+request.responseText, true);
                                    resolve('stop 1');
                                }
                            });

                            // Il resolve ritorna il comando di stoppare la macchina
                            if (is_cookie_ok <= 0 || (machine_status !== 'launched' && machine_status !== 'launch')) {
                                resolve('stop 2');
                            }

                            var is_interrupted = false;
                            var change_page = false;
                            var is_user_unavailable = false;
                            var is_company_unavailable = false;
                            current_url = current_url.replace(',', '%2C');

                            console.log('--- SCRAPER started ---');

                            var data_check_page = {
                                action: 'check_page',
                                client_id: ext,
                            };
                            var cp_resp = '';

                            $.ajax({
                                type: "POST",
                                url: ajax_url,
                                dataType: "JSON",
                                async: false,
                                data: data_check_page,
                                success: function (resp) {
                                    cp_resp = resp;
                                    if(cp_resp === 'no'){
                                        logger(ext, process_id, current_url,'check_page FINISH SCRAPING - ', false);
                                        resolve('stop 3');
                                    } else {
                                        logger(ext, process_id, current_url,'check_page AJAX ok', false);
                                    }
                                },
                                error: function (request, status, error) {
                                    logger(ext, process_id, current_url,'check_page AJAX ERROR - '+request.responseText, true);
                                    resolve('stop 4');
                                }
                            });

                            var resp_page = '';
                            var check_double_page = 0;
                            if (typeof cp_resp.page !== 'undefined' && cp_resp !== 'no') {
                                resp_page = cp_resp.page;
                                resp_page = resp_page.replace(',', '%2C');
                            }
                            if(cp_resp.status == 'update_name')
                                resp_page = cp_resp.url;
                            if (current_url.includes('/signup/'))
                                is_interrupted = true;
                            if (current_url.includes('/authwall'))
                                is_interrupted = true;
                            if (current_url.includes('/challenge/'))
                                is_interrupted = true;
                            if (current_url.includes('/uas/login'))
                                is_interrupted = true;
                            if (current_url.includes('/in/unavailable') || current_url.includes('/404/') ) {
                                is_user_unavailable = true;
                                check_double_page++;
                            }

                            if (cp_resp.status == 'maincompany' && is_user_unavailable) {
                                is_user_unavailable = false;
                                is_company_unavailable = true;
                            }

                            if (is_interrupted) {
                                var data_interrupted = {
                                    action: 'interrupted',
                                    client_id: ext,
                                    id_status: id_status
                                };

                                $.ajax({
                                    type: "POST",
                                    url: ajax_url,
                                    dataType: "JSON",
                                    async: false,
                                    data: data_interrupted,
                                    success: function (resp) {
                                        console.log(resp);
                                        logger(ext, process_id, current_url,'interrupted AJAX ok', true);
                                        resolve('stop 5');
                                    },
                                    error: function (request, status, error) {
                                        logger(ext, process_id, current_url,'interrupted AJAX ERROR - '+request.responseText, true);
                                        resolve('stop 6');
                                    }
                                });
                            }

                            var is_people = false;
                            var is_sales = false;

                            if (current_url.includes('/search/results/people'))
                                is_people = true;
                            if (current_url.includes('/sales/search'))
                                is_sales = true;
                            if (cp_resp.status === 'maincompany' && resp_page == '')
                                resp_page = cp_resp.url;

                            var resp_page_about = resp_page + 'about/';
                            if (current_url !== resp_page) {
                                if (resp_page_about === current_url)
                                    resp_page = resp_page_about;
                            }

                            console.log('wollolo1');
                            console.log(resp_page);
                            console.log(current_url);

                            var resp_page_last_char = resp_page.substr(resp_page.length - 1);
                            var current_url_last_char = current_url.substr(current_url.length - 1);
                            if(resp_page_last_char !== '/' && current_url_last_char == '/')
                                resp_page = resp_page + '/';

                            if (current_url !== resp_page && resp_page !== '') {
                                if (is_people) {
                                    let thispage_number = getUrlParameter('page');
                                    const resp_params = new URLSearchParams(resp_page);
                                    resp_page_number = resp_params.get('page');
                                    let this_ids_linkedin = getUrlParameter('currentCompany');
                                    let id_linkedin_founded = false;

                                    if(this_ids_linkedin.includes(cp_resp.id_linkedin))
                                        id_linkedin_founded = true;

                                    console.log('thispage_number ' + thispage_number);
                                    console.log('resp_page_number ' + resp_page_number);

                                    if (thispage_number != resp_page_number) {
                                        change_page = true;
                                        console.log('ispeople');
                                        logger(ext, process_id, current_url,'People: page changed from '+current_url+' to '+resp_page, false);
                                        resolve(resp_page);
                                    }

                                    if(!id_linkedin_founded){
                                        change_page = true;
                                        resolve(resp_page);
                                    }
                                } else {
                                    console.log(cp_resp.status);
                                    if ((cp_resp.status == 'maincompany' || cp_resp.status == 'update_name') && !is_company_unavailable && !is_user_unavailable) {
                                        logger(ext, process_id, current_url, 'maincompany check started', false);
                                        var redir_page = resp_page;
                                        redir_page = redir_page.replace('https://www.linkedin.com/', '');
                                        console.log('cur-url' + current_url);
                                        console.log('resp-page' + resp_page);
                                        //$( "div:contains('John')" )

                                        var datalet_url_changed = false;
                                        var change_url_ajax = false;
                                        // Verifica cambio URL tramite datalet
                                        $('[id^=datalet-bpr-guid-]').each(function () {
                                            if (!datalet_url_changed) {
                                                const text = removeExtraSpaces($(this).text());
                                                var company_url_suffix = resp_page;
                                                company_url_suffix = company_url_suffix.replace('https://www.linkedin.com/company/', '');
                                                company_url_suffix = company_url_suffix.replace('/about', '');
                                                company_url_suffix = company_url_suffix.replaceAll('/', '');
                                                company_url_suffix = encodeURIComponent(company_url_suffix);

                                                if (text.includes(company_url_suffix)) {
                                                    console.log('datalet change url');
                                                    datalet_url_changed = true;
                                                    change_url_ajax = true;
                                                }
                                            }
                                        });

                                        // Verifica cambio URL tramite meta apple-itunes-app
                                        if ($('meta[name="apple-itunes-app"]').length) {
                                            var meta = $('meta[name="apple-itunes-app"]').attr('content');
                                            if (meta.includes(redir_page)) {
                                                console.log('meta change url');
                                                change_url_ajax = true;
                                            }
                                        }

                                        console.log(change_url_ajax);

                                        if (change_url_ajax) {
                                            var data_company_url_changed = {
                                                action: 'company_url_changed',
                                                client_id: ext,
                                                company_id: cp_resp.id,
                                                company_url: resp_page,
                                                new_url: current_url,
                                            };

                                            resp_page = current_url;

                                            $.ajax({
                                                type: "POST",
                                                url: ajax_url,
                                                dataType: "JSON",
                                                async: false,
                                                data: data_company_url_changed,
                                                success: function (resp) {
                                                    console.log(resp);
                                                    logger(ext, process_id, current_url, 'company_url_changed AJAX ok', false);
                                                    resolve(resp_page);
                                                },
                                                error: function (request, status, error) {
                                                    logger(ext, process_id, current_url,'company_url_changed AJAX ERROR - '+request.responseText, true);
                                                    resolve('stop 7');
                                                }
                                            });


                                        }
                                    }

                                    if (is_user_unavailable) {
                                        var data_user_unavailable = {
                                            action: 'user_unavailable',
                                            client_id: ext,
                                            user_id: cp_resp.user_id,
                                        };

                                        $.ajax({
                                            type: "POST",
                                            url: ajax_url,
                                            dataType: "JSON",
                                            async: false,
                                            data: data_user_unavailable,
                                            success: function (resp) {
                                                console.log(resp);
                                                check_double_page++;
                                                logger(ext, process_id, current_url, 'user_unavailable AJAX ok', false);
                                            },
                                            error: function (request, status, error) {
                                                logger(ext, process_id, current_url,'user_unavailable AJAX ERROR - '+request.responseText, true);
                                                resolve('stop 8');
                                            }
                                        });
                                    }

                                    if (is_company_unavailable) {
                                        var data_company_unavailable = {
                                            action: 'company_unavailable',
                                            client_id: ext,
                                            company_id: cp_resp.id,
                                            company_url: resp_page
                                        };

                                        $.ajax({
                                            type: "POST",
                                            url: ajax_url,
                                            dataType: "JSON",
                                            async: false,
                                            data: data_company_unavailable,
                                            success: function (resp) {
                                                console.log(resp);
                                                check_double_page++;
                                                logger(ext, process_id, current_url, 'company_unavailable AJAX ok', false);
                                            },
                                            error: function (request, status, error) {
                                                logger(ext, process_id, current_url,'company_unavailable AJAX ERROR - '+request.responseText, true);
                                                resolve('stop 9');
                                            }
                                        });
                                    }

                                    if ((!is_user_unavailable && !is_company_unavailable && !is_sales) || check_double_page > 1) {
                                        change_page = true;
                                        logger(ext, process_id, current_url, 'Double check page redirect', false);
                                        resolve(resp_page);
                                    }

                                    if (resp_page.includes('redir_sales_scrap') && current_url.includes('/in/')) {
                                        var data_update_user_page = {
                                            action: 'update_user_page',
                                            client_id: ext,
                                            current_url: current_url,
                                            resp_page: resp_page,
                                        };

                                        $.ajax({
                                            type: "POST",
                                            url: ajax_url,
                                            dataType: "JSON",
                                            async: false,
                                            data: data_update_user_page,
                                            success: function (resp) {
                                                console.log(resp);
                                                resp_page = resp.page;
                                                logger(ext, process_id, current_url, 'update_user_page AJAX ok: current url '+current_url+' - resp_page '+resp_page, false);
                                            },
                                            error: function (request, status, error) {
                                                logger(ext, process_id, current_url,'update_user_page AJAX ERROR - '+request.responseText, true);
                                                resolve('stop 10');
                                            }
                                        });
                                    }

                                    if (resp_page !== current_url && cp_resp.status !== 'users_urls_sales_init' && resp_page.includes('filters:List') && !current_url.includes('type%3AFUNCTION')) {
                                        console.log(current_url);
                                        logger(ext, process_id, current_url, 'users urls sales init finished', false);
                                        resolve(resp_page);
                                    }
                                    //
                                    if (resp_page !== current_url && cp_resp.status == 'users_urls_sales' && current_url.includes('type%3AFUNCTION')){
                                        let check_page1 = resp_page.replace('page=1&', '');
                                        let check_current_url = current_url.split('&sessionId=');

                                        check_page1 = check_page1.replace('%2C', ',');
                                        check_current_url = check_current_url[0] + '&viewAllFilters=true';
                                        check_current_url = decodeURIComponent(check_current_url);

                                        if(check_page1 !== check_current_url) {
                                            console.log('wolloloy '+check_current_url);
                                            console.log('wollolox '+check_page1);
                                            resolve(resp_page);
                                        }
                                    }
                                }
                            }

                            console.log('here1');

                            if (cp_resp.status === 'maincompany' && resp_page !== current_url) {
                                change_page = true;
                                logger(ext, process_id, current_url, 'Main company redirect - current url '+current_url+' resp_page '+resp_page, false);
                                resolve(resp_page);
                            }

                            if (!change_page && !is_interrupted) {
                                console.log('here2');
                                console.log(cp_resp.status);
                                console.log(cp_resp);
                                /** START PURE SCRAPING **/
                                if (cp_resp.status == 'update_name' && cp_resp.ext == ext) {
                                    logger(ext, process_id, current_url, 'update_name started', false);
                                    var company_name = '';
                                    var company_name_founded = false;

                                    if ($('.org-top-card-primary-content__content-inner h1').length) {
                                        company_name = $('.org-top-card-primary-content__content-inner h1').attr('title');
                                        company_name_founded = true;
                                    }
                                    if ($('h1.org-top-card-summary__title').length) {
                                        company_name = $('.org-top-card-summary__title').attr('title');
                                        company_name_founded = true;
                                    }
                                    if ($('.org-top-card__primary-content h1').length) {
                                        company_name = $('.org-top-card__primary-content h1').attr('title');
                                        company_name_founded = true;
                                    }
                                    if ($('.artdeco-card h1 span').length) {
                                        company_name = $('.artdeco-card h1 span').text();
                                        company_name_founded = true;
                                    } else {
                                        if ($('.artdeco-card h1').length) {
                                            company_name = $('.artdeco-card h1').text();
                                            company_name_founded = true;
                                        }
                                    }

                                    if(company_name_founded && removeExtraSpaces(company_name) !== '')
                                        company_name = removeExtraSpaces(company_name);
                                    else
                                        logger(ext, process_id, current_url, 'company_name class not founded!', true);

                                    var data_company_update_name = {
                                        action: 'company_update_name',
                                        company_name: company_name,
                                        client_id: ext,
                                        id_company: cp_resp.id,
                                    };

                                    $.ajax({
                                        type: "POST",
                                        url: ajax_url,
                                        dataType: "JSON",
                                        async: false,
                                        data: data_company_update_name,
                                        success: function (resp) {
                                            console.log(resp);
                                            logger(ext, process_id, current_url, 'company_update_name AJAX ok', false);
                                        },
                                        error: function (request, status, error) {
                                            logger(ext, process_id, current_url,'company_update_name AJAX ERROR - '+request.responseText, true);
                                            resolve('stop 22');
                                        }
                                    });
                                }

                                if (cp_resp.status == 'maincompany' && cp_resp.ext == ext) {
                                    console.log('main enter');
                                    logger(ext, process_id, current_url, 'maincompany started', false);
                                    var url_company_users = '';
                                    var no_users = false;
                                    var company_name = '';
                                    var company_name_founded = false;

                                    var id_linkedin_full = '';
                                    var id_linkedin = '';
                                    var users_url_elem = '';
                                    var users_url_elem_founded = false;

                                    if ($('.org-top-card .org-top-card-secondary-content__connections a.ember-view:last-child').length) {
                                        users_url_elem = $('.org-top-card .org-top-card-secondary-content__connections a.ember-view:last-child');
                                        users_url_elem_founded = true;
                                        console.log('found url 1');
                                    }
                                    if ($('.org-top-card .org-top-card__right-col a.ember-view').length){
                                        users_url_elem = $('.org-top-card__right-col a.ember-view');
                                        users_url_elem_founded = true;
                                        console.log('found url 2');
                                    }
                                    if ($('.org-top-card-listing__summary a.ember-view').length) {
                                        users_url_elem = $('.org-top-card-listing__summary a.ember-view');
                                        users_url_elem_founded = true;
                                        console.log('found url 3');
                                    }
                                    if ($('.org-top-card a.ember-view').length) {
                                        users_url_elem = $('.org-top-card a.ember-view');
                                        users_url_elem_founded = true;
                                        console.log('found url 4');
                                    }
                                    if ($('a.ember-view[href*="search/results/people"]').length) {
                                        users_url_elem = $('a.ember-view[href*="search/results/people"]');
                                        users_url_elem_founded = true;
                                        console.log('found url 5');
                                    }

                                    if (users_url_elem_founded) {
                                        console.log(users_url_elem_founded);
                                        console.log(users_url_elem);
                                        url_company_users = users_url_elem.attr('href');
                                        // Link sito web esterno
                                        if (
                                            users_url_elem.hasClass('.org-top-card-primary-actions__external-link') ||
                                            users_url_elem.find('.org-top-card-primary-actions__external-link').length > 0 ||
                                            url_company_users.includes('http://') ||
                                            url_company_users.includes('https://') ||
                                            url_company_users == '#'
                                        )
                                            url_company_users = '';
                                    } else {
                                        logger(ext, process_id, current_url, 'users_url_elem class not founded!', true);
                                    }

                                    if (url_company_users != '') {
                                        // url_company_users = 'https://www.linkedin.com' + url_company_users + '&page=1';
                                        url_company_users = 'https://www.linkedin.com' + url_company_users;
                                        url_company_users = url_company_users.replace('&facetNetwork=%5B%22F%22%5D', '');
                                        url_company_users = url_company_users.replace('facetCurrentCompany', 'currentCompany');

                                        if (!url_company_users.includes('linkedin.com'))
                                            url_company_users = 'https://www.linkedin.com' + url_company_users;

                                        id_linkedin_full = url_company_users;
                                        id_linkedin_full = id_linkedin_full.replace('%5B%22', '');
                                        id_linkedin_full = id_linkedin_full.replace('%22%5D', '');
                                        id_linkedin_full = id_linkedin_full.replace('/search/results/people/?currentCompany=', '');
                                        id_linkedin_full = id_linkedin_full.replace('&facetNetwork=%5B%22F%22%5D&origin=COMPANY_PAGE_CANNED_SEARCH', '');
                                        id_linkedin_full = id_linkedin_full.replace('https://www.linkedin.com', '');
                                        id_linkedin_full = id_linkedin_full.replace('&origin=COMPANY_PAGE_CANNED_SEARCH', '');

                                        var id_linkedin_splitted = id_linkedin_full.split(",");
                                        id_linkedin = id_linkedin_splitted[0];

                                        console.log(cp_resp);

                                        var geo_full = cp_resp.geo;
                                        var geo_splitted = geo_full.split(",");
                                        var geo = geo_splitted[0];
                                        console.log('geo');
                                        console.log(geo_full);
                                        console.log(geo_splitted);
                                        console.log(geo);
                                        // geo = geo.replace('%5B%22', '');
                                        // geo = geo.replace('%22%5D', '');
                                        // geo = geo.replace('["', '');
                                        // geo = geo.replace('"]', '');

                                        url_company_users = url_company_users.replace('&origin=', '&geoUrn=' + geo_full + '&origin='); // Filtro solo italia
                                        // url_company_users = url_company_users.replace('%5B%22', '');
                                        // url_company_users = url_company_users.replace('%22%5D', '');
                                    } else {
                                        no_users = true;
                                        logger(ext, process_id, current_url, 'No users found', false);
                                    }

                                    if ($('.org-top-card-primary-content__content-inner h1').length) {
                                        company_name = $('.org-top-card-primary-content__content-inner h1').attr('title');
                                        company_name_founded = true;
                                    }
                                    if ($('h1.org-top-card-summary__title').length) {
                                        company_name = $('.org-top-card-summary__title').attr('title');
                                        company_name_founded = true;
                                    }
                                    if ($('.org-top-card__primary-content h1').length) {
                                        company_name = $('.org-top-card__primary-content h1').attr('title');
                                        company_name_founded = true;
                                    }
                                    if ($('.artdeco-card h1 span').length) {
                                        company_name = $('.artdeco-card h1 span').text();
                                        company_name_founded = true;
                                    } else {
                                        if ($('.artdeco-card h1').length) {
                                            company_name = $('.artdeco-card h1').text();
                                            company_name_founded = true;
                                        }
                                    }

                                    if(company_name_founded && removeExtraSpaces(company_name) !== '')
                                        company_name = removeExtraSpaces(company_name);
                                    else
                                        logger(ext, process_id, current_url, 'company_name class not founded!', true);

                                    if (no_users) {
                                        console.log('no_users')
                                        // NO USERS
                                        var data_company_no_users = {
                                            action: 'company_no_users',
                                            company_name: company_name,
                                            current_url: current_url,
                                            client_id: ext
                                        };

                                        $.ajax({
                                            type: "POST",
                                            url: ajax_url,
                                            dataType: "JSON",
                                            async: false,
                                            data: data_company_no_users,
                                            success: function (resp) {
                                                console.log(resp);
                                                logger(ext, process_id, current_url, 'company_no_users AJAX ok', false);
                                            },
                                            error: function (request, status, error) {
                                                logger(ext, process_id, current_url,'company_no_users AJAX ERROR - '+request.responseText, true);
                                                resolve('stop 11');
                                            }
                                        });
                                    } else {
                                        console.log('ok_users')
                                        // OK USERS

                                        var data_company_users = {
                                            action: 'company_users',
                                            company_name: company_name,
                                            current_url: current_url,
                                            client_id: ext,
                                            id_linkedin: id_linkedin,
                                            url_company_users: url_company_users,
                                        };

                                        console.log(data_company_users)

                                        $.ajax({
                                            type: "POST",
                                            url: ajax_url,
                                            dataType: "JSON",
                                            async: false,
                                            data: data_company_users,
                                            success: function (resp) {
                                                console.log(resp);
                                                console.log('company_users AJAX ok')
                                                logger(ext, process_id, current_url, 'company_users AJAX ok', false);

                                                if(resp == 'no update name'){
                                                    resolve('stop no_name');
                                                }
                                            },
                                            error: function (request, status, error) {
                                                console.log('error_1')
                                                logger(ext, process_id, current_url,'company_users AJAX ERROR - '+request.responseText, true);
                                                resolve('stop 12');
                                                // WOLLOLO
                                            }
                                        });
                                    }
                                }

                                console.log('after_users')

                                if (cp_resp.status == 'users_urls_sales' && cp_resp.ext == ext) {
                                    console.log('after_users_1')
                                    logger(ext, process_id, current_url, 'users_urls_sales started', false);
                                    $("body,html").animate({scrollTop: 0}, "slow");

                                    if($('#search-results-container').length) {
                                        $("#search-results-container").animate({scrollTop: 0}, "slow");
                                        setTimeout(function () {
                                            $("body,html").animate({scrollTop: $(document).height()}, "slow");
                                            $("#search-results-container").animate({scrollTop: $('#search-results-container')[0].scrollHeight}, "slow");
                                        }, 2000);
                                    }


                                    var id_company = cp_resp.id_company;

                                    if($('[class*="_search-empty-states"]').length){
                                        console.log('after_users_2')
                                        setTimeout(function () {
                                            var sales_function = $('form fieldset.pb4:last-child .artdeco-pill').attr('title');
                                            console.log('NON ci sono user');
                                            // NON CI SONO USERS
                                            var data_users_urls_sales_empty = {
                                                action: 'users_urls_sales_empty',
                                                client_id: ext,
                                                sales_function: sales_function,
                                                current_url: current_url,
                                                id_company: id_company,
                                            };

                                            console.log(data_users_urls_sales_empty);

                                            $.ajax({
                                                type: "POST",
                                                url: ajax_url,
                                                dataType: "JSON",
                                                async: false,
                                                data: data_users_urls_sales_empty,
                                                success: function (resp) {
                                                    console.log(resp);
                                                    logger(ext, process_id, current_url, 'users_urls_sales_empty AJAX ok', false);
                                                },
                                                error: function (request, status, error) {
                                                    logger(ext, process_id, current_url, 'users_urls_sales_empty AJAX ERROR - ' + request.responseText, true);
                                                    // resolve('stop 13');
                                                }
                                            });
                                        }, 5000);
                                    } else {
                                        // CI SONO USERS
                                        console.log('ci sono user');
                                        setTimeout(function () {
                                            // var sales_function = $('form fieldset.pb4:last-child .artdeco-pill').attr('title');
                                            var sales_function = $('[data-x-search-filter="FUNCTION"] .artdeco-pill').attr('title');
                                            const total_pages = parseInt( $('.artdeco-pagination__pages .artdeco-pagination__indicator:last-child button span').text() );
                                            var current_page = getUrlParameter('page');
                                            if (current_page === null || typeof current_page === "undefined")
                                                current_page = 1;
                                            var sales_users = [];

                                            // var sales_function = $('[data-test-search-filter="FUNCTION"] .artdeco-pill').attr('title');
                                            console.log(sales_function);
                                            var counter_sales_users = 0;
                                            var page_counter = parseInt(current_page);
                                            var next_url = '';

                                            // if($('.search-results__pagination-list li:last-child button').length <= 0)
                                            //     logger(ext, process_id, current_url, 'total_pages Class not found!', true);
                                            // if($('[data-test-filter-code="F"] .control-pill__text span').length <= 0)
                                            //     logger(ext, process_id, current_url, 'sales_function Class not found!', true);
                                            // if($('.search-results__result-list .search-results__result-item').length <= 0)
                                            //     logger(ext, process_id, current_url, 'Each users Class not found!', true);
                                            if($('#search-results-container [class*="search-results"] li.artdeco-list__item').length <= 0)
                                                logger(ext, process_id, current_url, 'Each users Class not found!', true);
                                            if($('.artdeco-pagination__pages .artdeco-pagination__indicator:nth-of-type(10)').length <= 0)
                                                logger(ext, process_id, current_url, 'SN: Total pages Class not found!', true);

                                            console.log('-----------');
                                            console.log('current page '+current_page);
                                            console.log('total page '+total_pages);
                                            console.log('current_url '+current_url);
                                            if (current_page < total_pages) {
                                                page_counter++;
                                                if(current_url.includes('page=')) {
                                                    next_url = current_url.replace('page=' + current_page, 'page=' + page_counter);
                                                } else {
                                                    next_url = current_url.replace('search/people?', 'search/people?page=' + page_counter + '&');
                                                }
                                            }

                                            console.log('next url '+next_url);
                                            console.log('-----------');

                                            $('#search-results-container [class*="search-results"] li.artdeco-list__item').each(function () {
                                                var fullname = removeExtraSpaces($(this).find('[data-anonymize="person-name"]').text());
                                                var firstname = fullname.split(' ').slice(0, -1).join(' ');
                                                var lastname = fullname.split(' ').slice(-1).join(' ');
                                                var photo = $(this).find('[data-control-name="view_profile_via_result_image"] img').attr('src');
                                                var url_sales = $(this).find('[data-anonymize="person-name"]').attr('href');
                                                var role = removeExtraSpaces( $(this).find('.artdeco-entity-lockup__subtitle span:first-child').text() );
                                                var location = removeExtraSpaces( $(this).find('.artdeco-entity-lockup__caption span:first-child').text() );

                                                // var url_sales_splitted = url_sales.split(',NAME_SEARCH');
                                                // var u_url = url_sales_splitted[0].replace('/sales/people/', '');
                                                // u_url = 'https://www.linkedin.com/in/' + u_url + '?redir_sales_scrap';

                                                if($(this).find('[data-anonymize="person-name"]').length <= 0)
                                                    logger(ext, process_id, current_url, 'fullname/url_sales Class not found!', true);
                                                if($(this).find('[data-anonymize="headshot-photo"]').length <= 0)
                                                    logger(ext, process_id, current_url, 'photo Class not found!', true);
                                                if($(this).find('.artdeco-entity-lockup__subtitle span:first-child').length <= 0)
                                                    logger(ext, process_id, current_url, 'role Class not found!', true);
                                                if($(this).find('.artdeco-entity-lockup__caption span:first-child').length <= 0)
                                                    logger(ext, process_id, current_url, 'location Class not found!', true);

                                                if (photo.includes('data:image'))
                                                    photo = '';
                                                else
                                                    photo = removeExtraSpaces(photo);


                                                sales_users[counter_sales_users] = [
                                                    ['fullname', fullname],
                                                    ['firstname', firstname],
                                                    ['lastname', lastname],
                                                    ['location', location],
                                                    ['photo', photo],
                                                    ['role', role],
                                                    ['url', url_sales]
                                                ];

                                                counter_sales_users++;
                                            });
                                            console.log('sales users');
                                            console.log(sales_users);

                                            var data_users_urls_sales = {
                                                action: 'users_urls_sales',
                                                client_id: ext,
                                                total_pages: total_pages,
                                                current_page: current_page,
                                                sales_users: sales_users,
                                                sales_function: sales_function,
                                                current_url: current_url,
                                                id_company: id_company,
                                                next_url: next_url,
                                            };

                                            console.log(data_users_urls_sales);

                                            $.ajax({
                                                type: "POST",
                                                url: ajax_url,
                                                dataType: "JSON",
                                                async: false,
                                                data: data_users_urls_sales,
                                                success: function (resp) {
                                                    console.log(resp);
                                                    logger(ext, process_id, current_url,'users_urls_sales AJAX ok', false);
                                                },
                                                error: function (request, status, error) {
                                                    logger(ext, process_id, current_url,'users_urls_sales AJAX ERROR - '+request.responseText, true);
                                                    // resolve('stop 13');
                                                }
                                            });
                                        }, 5000);
                                    }
                                }

                                if (cp_resp.status == 'users_urls_sales_init' && cp_resp.ext == ext) {
                                    logger(ext, process_id, current_url,'users_urls_sales_init started', false);
                                    // Sales navigator (inside)
                                    // if (
                                    //     (current_url.includes('search/results/people') && resp_page.includes('sales/search')) ||
                                    //     (current_url.includes('sales/search/people') && resp_page.includes('sales/search'))
                                    // ) {
                                    //     logger(ext, process_id, current_url,'users_urls_sales_init change page', false);
                                    //     resolve(resp_page);
                                    // } else {
                                        // $("body,html").animate({scrollTop: $(document).height()}, "slow");
                                        // var container = $('form [data-test-search-filter="FUNCTION"]');
                                        var container = $('form fieldset.pb4:nth-child(2)');

                                        if(container.length <= 0)
                                            logger(ext, process_id, current_url,'functions container Class not found', true);
                                        // var base_url = current_url;
                                        // var button = container.find('button[data-test-search-filter="container-toggle"]');
                                        var button = container.find('button.artdeco-button');
                                        if (button.attr('aria-expanded') == 'false') {
                                            console.log('not clicked');
                                            button.click();
                                            button[0].click();
                                        }

                                        console.log(container.find('ul li').length);

                                        if (container.find('ul li').length > 0) {
                                            // var sales_functions = [];
                                            // var sales_counter = 0;
                                            // container.find('ul li').each(function () {
                                            //     var function_id = $(this).find("[class^='_include-button'],[class*=' _include-button']").attr('data-test-search-filter-typeahead-suggestion');
                                            //     console.log('function_id');
                                            //     console.log(function_id);
                                            //     function_id = function_id.replace('include-', '');
                                            //     // var function_url = base_url + '&doFetchHeroCard=false&functionIncluded=' + function_id + '&logHistory=false&page=1'
                                            //     var function_url = 'https://www.linkedin.com/sales/search/people?page=1&query=' +
                                            //         '(filters:List((type:CURRENT_COMPANY,values:List((id:' + cp_resp.id_linkedin + ',selectionType:INCLUDED))),' +
                                            //         '(type:FUNCTION,values:List((id:' + function_id + ',selectionType:INCLUDED)))))&viewAllFilters=true';
                                            //
                                            //     var function_name = removeExtraSpaces( $(this).find('span.t-14').clone().children().remove().end().text() );
                                            //     sales_functions[sales_counter] = [
                                            //         ['url', function_url],
                                            //         ['name', function_name],
                                            //     ];
                                            //     sales_counter++;
                                            // })

                                            var data_sales_functions = {
                                                action: 'sales_functions',
                                                current_url: current_url,
                                                client_id: ext,
                                                id_linkedin: cp_resp.id_linkedin,
                                            };

                                            $.ajax({
                                                type: "POST",
                                                url: ajax_url,
                                                dataType: "JSON",
                                                async: false,
                                                data: data_sales_functions,
                                                success: function (resp) {
                                                    console.log(resp);
                                                    logger(ext, process_id, current_url,'sales_functions AJAX ok', false);
                                                },
                                                error: function (request, status, error) {
                                                    logger(ext, process_id, current_url,'sales_functions AJAX ERROR - '+request.responseText, true);
                                                    resolve('stop 14');
                                                }
                                            });
                                        }
                                    // }
                                }

                                if (cp_resp.status == 'users_urls' && cp_resp.ext == ext) {

                                    $("body,html").animate({scrollTop: $(document).height()}, "slow");

                                    console.log('HERE 2');


                                    setTimeout(function () {
                                        var total_users = '';
                                        var total_users_founded = false;
                                        if ($('.search-results-page.core-rail .pb2:first-child').length) {
                                            total_users = $('.search-results-page.core-rail .pb2:first-child').text();
                                            total_users_founded = true;
                                        }
                                        if ($('.search-results-container .pb2.t-black--light.t-14').length) {
                                            total_users = $('.search-results-container .pb2.t-black--light.t-14').text();
                                            total_users_founded = true;
                                        }

                                        if(total_users_founded) {
                                            total_users = removeExtraSpaces(total_users.replace(' risultati', ''));
                                            total_users = removeExtraSpaces(total_users.replace('Circa ', ''));
                                            total_users = removeExtraSpaces(total_users.replace('.', ''));

                                            total_users = parseInt(total_users);
                                            var total_pages = Math.ceil(total_users / 10);

                                            if (total_pages > 100)
                                                total_pages = 100;
                                        } else
                                            logger(ext, process_id, current_url,'total_users Class not found', true);

                                        console.log('TOTAL USERS: '+total_users);

                                        var company_id_linkedin_full = getUrlParameter('currentCompany');
                                        var company_id_linkedin_splitted = company_id_linkedin_full.split(",");
                                        var company_id_linkedin = company_id_linkedin_splitted[0];
                                        company_id_linkedin = company_id_linkedin.replace('%5B%22', '');
                                        company_id_linkedin = company_id_linkedin.replace('%22%5D', '');
                                        company_id_linkedin = company_id_linkedin.replace('[', '');
                                        company_id_linkedin = company_id_linkedin.replace(']', '');
                                        company_id_linkedin = company_id_linkedin.replace('"', '');
                                        company_id_linkedin = company_id_linkedin.replace('"', '');

                                        var geo_full = cp_resp.geo;
                                        var geo_splitted = geo_full.split(",");
                                        var geo = geo_splitted[0];

                                        console.log('users urls');
                                        console.log(cp_resp);

                                        console.log('geo');
                                        console.log(geo_full);
                                        console.log(geo_splitted);
                                        console.log(geo);

                                        var current_page = getUrlParameter('page');
                                        if (current_page === null || typeof current_page === "undefined")
                                            current_page = 1;
                                        else
                                            current_page = parseInt(current_page);

                                        var users = [];
                                        var next_url = '';

                                        if ($('.artdeco-empty-state').length <= 0) {
                                            for (var i = 1; i <= 10; i++) {
                                                if ($('ul.reusable-search__entity-result-list li.reusable-search__result-container:nth-child(' + i + ')').length) {
                                                    var result = $('ul.reusable-search__entity-result-list li.reusable-search__result-container:nth-child(' + i + ')');
                                                    var fullname = '';
                                                    var fullname_founded = false;
                                                    if(result.find('.app-aware-link span[aria-hidden="true"]').length){
                                                        fullname = result.find('.app-aware-link span[aria-hidden="true"]').text();
                                                        fullname_founded = true;
                                                    }
                                                    if (fullname === '' || !fullname_founded) {
                                                        if(result.find('.app-aware-link').length)
                                                            fullname = removeExtraSpaces(result.find('.app-aware-link').text());
                                                        else
                                                            fullname_founded = false;
                                                    }
                                                    if(!fullname_founded)
                                                        logger(ext, process_id, current_url,'fullname/url Class not found', true);

                                                    var firstname = fullname.split(' ').slice(0, -1).join(' ');
                                                    var lastname = fullname.split(' ').slice(-1).join(' ');
                                                    var location = removeExtraSpaces(result.find('.linked-area .entity-result__secondary-subtitle').text());

                                                    if(result.find('.linked-area .entity-result__secondary-subtitle').length <= 0)
                                                        logger(ext, process_id, current_url,'location Class not found', true);

                                                    var photo = '';
                                                    if (result.find('.ivm-view-attr__img--centered').length)
                                                        photo = result.find('.ivm-view-attr__img--centered').attr('src');
                                                    else
                                                        logger(ext, process_id, current_url,'photo Class not found', true);

                                                    var url = result.find('.app-aware-link').attr('href') + '/';
                                                    var role_all = '';
                                                    var role = '';

                                                    if(result.find('.linked-area .entity-result__primary-subtitle').length){
                                                        role_all = removeExtraSpaces(result.find('.linked-area .entity-result__primary-subtitle').text());
                                                        role_all = removeExtraSpaces(role_all);
                                                        var split_role = role_all.split(' presso ');
                                                        if (split_role.length == 1)
                                                            split_role = role_all.split('@');
                                                        if (split_role.length == 1)
                                                            split_role = role_all.split(' at ');
                                                        if (split_role.length == 1)
                                                            split_role = role_all.split(', ');
                                                        role = split_role[0];
                                                    } else {
                                                        logger(ext, process_id, current_url, 'photo Class not found', true);
                                                    }

                                                    if (typeof photo === "undefined")
                                                        photo = '';

                                                    users[i - 1] = [
                                                        ['fullname', fullname],
                                                        ['firstname', firstname],
                                                        ['lastname', lastname],
                                                        ['location', location],
                                                        ['photo', photo],
                                                        ['role', role],
                                                        ['url', url],
                                                    ];
                                                }
                                            }

                                            if (current_page < total_pages) {
                                                current_page++;
                                                next_url = 'https://www.linkedin.com/search/results/people/?currentCompany=' + company_id_linkedin_full + "&geoUrn=" + geo_full + "&origin=COMPANY_PAGE_CANNED_SEARCH&page=" + current_page;
                                            }

                                            console.log('NEXT URL: ' + next_url);

                                            if (Number.isNaN(total_users)) {
                                                var lastpage = $('.artdeco-pagination__indicator--number:last-child').attr('data-test-pagination-page-btn');
                                                console.log(lastpage);
                                                var data_company_users_finish = {
                                                    action: 'company_users_finish',
                                                    client_id: ext,
                                                    company_name: company_name,
                                                    id_linkedin: company_id_linkedin,
                                                    current_page: current_page,
                                                    lastpage: lastpage
                                                };

                                                $.ajax({
                                                    type: "POST",
                                                    url: ajax_url,
                                                    dataType: "JSON",
                                                    async: false,
                                                    data: data_company_users_finish,
                                                    success: function (resp) {
                                                        console.log(resp);
                                                        logger(ext, process_id, current_url, 'company_users_finish AJAX ok', false);
                                                    },
                                                    error: function (request, status, error) {
                                                        logger(ext, process_id, current_url,'company_users_finish AJAX ERROR - '+request.responseText, true);
                                                        resolve('stop 15');
                                                    }
                                                });
                                            } else {
                                                var is_sales_navigator = false;
                                                if ($('.search-nec__banner-card').length > 0 && total_users > 1000)
                                                    is_sales_navigator = true;

                                                if (!is_sales_navigator) {
                                                    logger(ext, process_id, current_url, 'not Sales navigator and users less than 1000 (Sales navigator disabled)', false);

                                                    var data_users_urls = {
                                                        action: 'users_urls',
                                                        client_id: ext,
                                                        id_linkedin: company_id_linkedin,
                                                        total_users: total_users,
                                                        total_users_pages: total_pages,
                                                        current_page: current_page,
                                                        next_url: next_url,
                                                        users: users,
                                                    }

                                                    console.log(users);

                                                    $.ajax({
                                                        type: "POST",
                                                        url: ajax_url,
                                                        dataType: "JSON",
                                                        async: false,
                                                        data: data_users_urls,
                                                        success: function (res) {
                                                            console.log(res);
                                                            logger(ext, process_id, current_url, 'users_urls AJAX ok', false);
                                                        },
                                                        error: function (request, status, error) {
                                                            logger(ext, process_id, current_url,'users_urls AJAX ERROR - '+request.responseText, true);
                                                            resolve('stop 16');
                                                        }
                                                    });
                                                } else {
                                                    logger(ext, process_id, current_url, 'is Sales navigator', false);
                                                    // var sales_nav_url = $('.search-nec__banner-card a.inverse-link-light-background-without-hover-state').attr('href');
                                                    var sales_nav_url = 'https://www.linkedin.com/sales/search/people?query=(filters:List((type:CURRENT_COMPANY,values:List((id:'+company_id_linkedin+',selectionType:INCLUDED)))))&viewAllFilters=true';

                                                    // if($('.search-nec__banner-card a.inverse-link-light-background-without-hover-state').length <= 0)
                                                    //     logger(ext, process_id, current_url,'sales_nav_url Class not found', true);
                                                    console.log(sales_nav_url);

                                                    var data_users_urls_sales_init = {
                                                        action: 'users_urls_sales_init',
                                                        client_id: ext,
                                                        id_linkedin: company_id_linkedin,
                                                        total_users: total_users,
                                                        url: sales_nav_url
                                                    }

                                                    $.ajax({
                                                        type: "POST",
                                                        url: ajax_url,
                                                        dataType: "JSON",
                                                        async: false,
                                                        data: data_users_urls_sales_init,
                                                        success: function (res) {
                                                            console.log(res);
                                                            if(res !== 'no')
                                                                resolve(res);
                                                            else
                                                                logger(ext, process_id, current_url, 'users_urls_sales_init COMPANY NOT FOUND', true);

                                                            logger(ext, process_id, current_url, 'users_urls_sales_init AJAX ok', false);
                                                        },
                                                        error: function (request, status, error) {
                                                            logger(ext, process_id, current_url,'users_urls_sales_init AJAX ERROR - '+request.responseText, true);
                                                            resolve('stop 17');
                                                        }
                                                    });
                                                }
                                            }
                                        } else {
                                            logger(ext, process_id, current_url, 'company_no_users started', false);
                                            var company_id_linkedin_full = getUrlParameter('currentCompany');
                                            var company_id_linkedin_splitted = company_id_linkedin_full.split(",");
                                            var id_linkedin = company_id_linkedin_splitted[0];
                                            id_linkedin = id_linkedin.replace('%5B%22', '');
                                            id_linkedin = id_linkedin.replace('%22%5D', '');
                                            id_linkedin = id_linkedin.replace('["', '');
                                            id_linkedin = id_linkedin.replace('"]', '');

                                            var data_company_no_users = {
                                                action: 'company_no_users',
                                                company_name: company_name,
                                                id_linkedin: id_linkedin,
                                                current_url: current_url,
                                                client_id: ext
                                            };

                                            $.ajax({
                                                type: "POST",
                                                url: ajax_url,
                                                dataType: "JSON",
                                                async: false,
                                                data: data_company_no_users,
                                                success: function (resp) {
                                                    console.log(resp);
                                                    logger(ext, process_id, current_url, 'company_no_users AJAX ok', false);
                                                },
                                                error: function (request, status, error) {
                                                    logger(ext, process_id, current_url,'company_no_users AJAX ERROR - '+request.responseText, true);
                                                    resolve('stop 18');
                                                }
                                            });
                                        }
                                    }, 5000);
                                }

                                if (!is_user_unavailable) {
                                    if (cp_resp.status == 'details' && !current_url.includes('redir_sales_scrap')) {
                                        logger(ext, process_id, current_url, 'details started', false);
                                        $("body,html").animate({scrollTop: $(document).height()}, "slow");

                                        if ($('#line-clamp-show-more-button').length){
                                            $('#line-clamp-show-more-button').click();
                                            $('#line-clamp-show-more-button')[0].click();
                                        }
                                        if ($('.inline-show-more-text__button').length){
                                            $('.inline-show-more-text__button').click();
                                            $('.inline-show-more-text__button')[0].click();
                                        }

                                        var connections = '';
                                        if ($('.ph5.pb5 .pv-top-card--list.pv-top-card--list-bullet li.inline-block:nth-child(2)').length) {
                                            connection = $('.ph5.pb5 .pv-top-card--list.pv-top-card--list-bullet li.inline-block:nth-child(2)').text();
                                        }
                                        if ($('section.pv-top-card .pv-top-card--list-bullet .t-bold').length) {
                                            connection = $('section.pv-top-card .pv-top-card--list-bullet .t-bold').text();
                                        }
                                        connections = removeExtraSpaces(connections);
                                        connections = connections.replace(' collegamenti', '');
                                        connections = connections.replace(' collegamento', '');

                                        var role_all = '';
                                        var role = '';
                                        var role_founded = false;
                                        if ($('.ph5.pb5 h2.t-black.t-normal.t-18').length) {
                                            role_all = $('.ph5.pb5 h2.t-black.t-normal.t-18').text();
                                            role_founded = true;
                                        }
                                        if ($('section.pv-top-card .text-body-medium').length){
                                            role_all = $('section.pv-top-card .text-body-medium').text();
                                            role_founded = true;
                                        }

                                        if(role_founded){
                                            role_all = removeExtraSpaces(role_all);
                                            var split_role = role_all.split(' presso ');
                                            if (split_role.length == 1)
                                                split_role = role_all.split('@');
                                            if (split_role.length == 1)
                                                split_role = role_all.split(' at ');
                                            if (split_role.length == 1)
                                                split_role = role_all.split(', ');
                                            role = split_role[0];
                                        } else {
                                            logger(ext, process_id, current_url, 'role Class not found', true);
                                        }

                                        var connections_open = $('a[data-control-name=topcard_view_all_connections]').length;
                                        var informations = '';
                                        var informations_found = false;

                                        if($('.pv-about-section .pv-about__summary-text .lt-line-clamp__line').length) {
                                            $('.pv-about-section .pv-about__summary-text .lt-line-clamp__line').each(function () {
                                                informations = informations + ' ' + removeExtraSpaces($(this).text());
                                            });
                                            informations_found = true;
                                        }
                                        if($('.pv-about-section .inline-show-more-text').length) {
                                            informations = $('.pv-about-section .inline-show-more-text').text();
                                            informations_found = true;
                                        }
                                        if(!informations_found){
                                            logger(ext, process_id, current_url, 'no info found', false);
                                        }
                                        var user_url = location.href;
                                        var fullname = '';
                                        var fullname_founded = false;
                                        if( $('.pv-top-card .t-24.t-black.t-normal').length ) {
                                            fullname = $('.pv-top-card .t-24.t-black.t-normal').text();
                                            fullname_founded = true;
                                        }
                                        if( $('section.pv-top-card h1.text-heading-xlarge').length ) {
                                            fullname = $('section.pv-top-card h1.text-heading-xlarge').text();
                                            fullname_founded = true;
                                        }

                                        if(fullname_founded)
                                            fullname = removeExtraSpaces(fullname);
                                        else
                                            logger(ext, process_id, current_url, 'fullname Class not found', true);

                                        var firstname = fullname.split(' ').slice(0, -1).join(' ');
                                        var lastname = fullname.split(' ').slice(-1).join(' ');
                                        var user_location = '';
                                        var photo = '';

                                        if($('.pv-top-card .pv-text-details__left-panel .text-body-small.inline').length)
                                            user_location = removeExtraSpaces( $('.pv-top-card .pv-text-details__left-panel .text-body-small.inline').text() );
                                        else
                                            logger(ext, process_id, current_url, 'user_location Class not found', true);

                                        if($('.pv-top-card .pv-top-card__photo img').length)
                                            photo = $('.pv-top-card .pv-top-card__photo img').attr('src');
                                        else
                                            logger(ext, process_id, current_url, 'photo Class not found', true);

                                        var data_details = {
                                            action: 'user_details',
                                            client_id: ext,
                                            connections: connections,
                                            role: role,
                                            connections_open: connections_open,
                                            info: informations,
                                            user_url: user_url,
                                            firstname: firstname,
                                            lastname: lastname,
                                            location: user_location,
                                            photo: photo,
                                            user_id: cp_resp.user_id,
                                        };

                                        $.ajax({
                                            type: "POST",
                                            url: ajax_url,
                                            dataType: "JSON",
                                            async: false,
                                            data: data_details,
                                            success: function (resp) {
                                                console.log(resp);
                                                logger(ext, process_id, current_url, 'user_details AJAX ok', false);
                                            },
                                            error: function (request, status, error) {
                                                logger(ext, process_id, current_url,'user_details AJAX ERROR - '+request.responseText, true);
                                                resolve('stop 19');
                                            }
                                        });
                                    }

                                    if (cp_resp.status == 'computed') {
                                        logger(ext, process_id, current_url,'computed started', false);
                                        var no_more_posts = false;
                                        var computed_stop_scrolling = false;
                                        var posts = [];

                                        setTimeout(function () {
                                            var postcount = 0;
                                            var date_lastpost = '';
                                            var followers = removeExtraSpaces($('.display-flex.justify-space-between.t-12.t-bold.t-black--light.mt3 div:nth-child(2)').text());

                                            if($('.display-flex.justify-space-between.t-12.t-bold.t-black--light.mt3 div:nth-child(2)').length <= 0)
                                                logger(ext, process_id, current_url,'followers Class not found', true);

                                            if ($('.artdeco-empty-state').length || $('.pv-recent-activity-detail__core-rail .occludable-update.ember-view').length <= 0) {
                                                logger(ext, process_id, current_url,'computed - nothing to analyze', false);
                                                var data_no_computed = {
                                                    action: 'computed',
                                                    client_id: ext,
                                                    user_id: cp_resp.user_id,
                                                    postcount: postcount,
                                                    followers: followers,
                                                    data_lastpost: date_lastpost,
                                                };
                                                $.ajax({
                                                    type: "POST",
                                                    url: ajax_url,
                                                    dataType: "JSON",
                                                    async: false,
                                                    data: data_no_computed,
                                                    success: function (resp) {
                                                        console.log(resp);
                                                        logger(ext, process_id, current_url,'computed (no computed to analyze) AJAX ok', false);
                                                    },
                                                    error: function (request, status, error) {
                                                        logger(ext, process_id, current_url,'computed (no computed to analyze) AJAX ERROR - '+request.responseText, true);
                                                        resolve('stop 20');
                                                    }
                                                });
                                            } else {
                                                if (!computed_stop_scrolling)
                                                    $("body,html").animate({scrollTop: $(document).height()}, "slow");

                                                console.log('stop scrolling: '+computed_stop_scrolling);

                                                var found = false;
                                                var stop_posts = false;
                                                var load_more_button = $('.display-flex.mt4.visually-hidden button .artdeco-button__text').length;
                                                if(load_more_button <= 0)
                                                    no_more_posts = true;

                                                $('.pv-recent-activity-detail__core-rail .occludable-update.ember-view').each(function () {
                                                    var time = $(this).find('.feed-shared-actor__meta .feed-shared-actor__sub-description.t-12.t-normal.t-black--light span.visually-hidden').text();

                                                    if($(this).find('.feed-shared-actor__meta .feed-shared-actor__sub-description.t-12.t-normal.t-black--light span.visually-hidden').length <= 0)
                                                        logger(ext, process_id, current_url,'time Class not found', true);

                                                    if (no_more_posts && !stop_posts) {
                                                        computed_stop_scrolling = true;
                                                        found = true;
                                                        stop_posts = true;
                                                        console.log('fine posts');
                                                    }

                                                    if (time.includes('ann') && !stop_posts) {
                                                        computed_stop_scrolling = true;
                                                        found = true;
                                                        stop_posts = true;
                                                    } else {
                                                        posts.push($(this).attr('id'));
                                                    }

                                                    if (found) {
                                                        found = false;
                                                        date_lastpost = time;
                                                    }
                                                });

                                                postcount = posts.length;

                                                if (computed_stop_scrolling) {
                                                    logger(ext, process_id, current_url,'computed - stop scrolling', false);
                                                    var data_computed = {
                                                        action: 'computed',
                                                        client_id: ext,
                                                        user_id: cp_resp.user_id,
                                                        postcount: postcount,
                                                        followers: followers,
                                                        data_lastpost: date_lastpost,
                                                    };

                                                    console.log(data_computed);
                                                    $.ajax({
                                                        type: "POST",
                                                        url: ajax_url,
                                                        dataType: "JSON",
                                                        async: false,
                                                        data: data_computed,
                                                        success: function (resp) {
                                                            console.log(resp);
                                                            logger(ext, process_id, current_url,'computed AJAX ok', false);
                                                        },
                                                        error: function (request, status, error) {
                                                            logger(ext, process_id, current_url,'computed AJAX ERROR - '+request.responseText, true);
                                                            resolve('stop 21');
                                                        }
                                                    });
                                                }
                                            }
                                        }, 5000);
                                    }
                                }
                                /** END PURE SCRAPING **/
                            }

                        }, 3500);
                    });

                }

                return launch_main_interval().then(function (x) {
                    return x;
                });
            }, {ext, authcookie, id_status, end_time, process_id});

            // Comando ritornato dal promise resolve
            console.log(page_result);
            node_logger(ext, process_id, pup_page, 'Finish with page result: ' + page_result);

            if(page_result.includes('stop ')){
                await browser.close();
            }
            if(page_result.includes('https://') || page_result.includes('http://')){
                node_logger(ext, process_id, pup_page, 'Page changed from '+ pup_page + ' to ' + page_result);
                pup_page = page_result;
            }
        }
    } catch(err) {
        console.log(err);
        node_logger(ext, process_id, pup_page, err.message, true);
        await browser.close();
    }
})();
