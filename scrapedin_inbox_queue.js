// node scrapedin_inbox_queue.js --messages='{"1":{"messages":[{"id_mex":6,"id_interlocutor":573914,"message":"come stai","url":"https:\/\/www.linkedin.com\/in\/simone-penza\/","ext":1}],"cookie":"AQEDAQEjVDYBiwwEAAABgT0XGMQAAAGBYSOcxFYAphVcJ280hMV_HTysrua6a-CHii-YrruTo1CFr8r6YMOw0gZP44xCgnfjPLleBYQZv3M_0mh6WjQ3Mz94u1c4M-06yfypRkmuHQVi_5J4kqUGa_s-"}}'


const puppeteer = require('puppeteer');
const fs = require('fs');
const util = require('util');

function getArgs () {
    const args = {};
    process.argv
        .slice(2, process.argv.length)
        .forEach( arg => {
            // long arg
            if (arg.slice(0,2) === '--') {
                const longArg = arg.split('=');
                const longArgFlag = longArg[0].slice(2,longArg[0].length);
                const longArgValue = longArg.length > 1 ? longArg[1] : true;
                args[longArgFlag] = longArgValue;
            }
            // flags
            else if (arg[0] === '-') {
                const flags = arg.slice(1,arg.length).split('');
                flags.forEach(flag => {
                    args[flag] = true;
                });
            }
        });
    return args;
}

(async () => {

    var is_debug = false;
    var jquery_path = 'node_modules/jquery/dist/jquery.js'

    puppeteer_options = {};

    if(__dirname.startsWith('/Users/')) {
        is_debug = true;
        console.log('ScrapedIn running locally - automatic "is_debug" true');
    }
    puppeteer_options = {
        ignoreHTTPSErrors: true,
        headless: false,
        devtools: true,
        args: ["--no-sandbox", '--disable-web-security']
    };

    if(!is_debug) {
        puppeteer_options = {
            ignoreHTTPSErrors: true,
            headless: true,
            devtools: false,
            defaultViewport: { width: 1600, height: 800 },
            args: ["--no-sandbox",  '--disable-web-security']
        };
        jquery_path = '/var/www/puppeteer/node_modules/jquery/dist/jquery.js'
    }

    const args = getArgs();

    var messages = args.messages;
    var authcookie = '';

    // console.log(messages);
    // console.log('-------');
    // console.log(JSON.parse(messages));

    messages = JSON.parse(messages);

    for(var key in messages){
        
        var user_messages = messages[key];

        console.log(messages[key]['cookie'])

        const browser = await puppeteer.launch(puppeteer_options);

        const page = await browser.newPage();

        authcookie = user_messages['cookie'];

        // console.log(user_messages)

        // console.log(authcookie)
        // console.log('********');

        const cookie = {
            name: 'li_at',
            value: authcookie,
            domain: '.www.linkedin.com',
            url: 'https://www.linkedin.com',
            path: '/',
            httpOnly: true,
            secure: true
        }

        console.log(cookie)

        try {
            await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36");
            await page.setCookie(cookie)
            await page.setBypassCSP(true);

        } catch(err) {
            console.log(err);
        }

        for( var key2 in user_messages['messages'] ){

            var user_mex = user_messages['messages'][key2];

            console.log(user_mex.url)

            var
            id_user = user_mex.ext,
            id_mex = user_mex.id_mex;

            // console.log('-------');

            console.log('Opening page ...');
            try {
                await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36");
                await page.setCookie(cookie)
                await page.setBypassCSP(true);
                await page.goto(user_mex.url, {timeout: 180000});
                async function injectFile(page, filePath) {
                    let contents = await new Promise((resolve, reject) => {
                        fs.readFile(filePath, 'utf8', (err, data) => {
                            if (err) return reject(err);
                            resolve(data);
                        });
                    });
                    contents += `//# sourceURL=` + filePath.replace(/\n/g,'');
                    return page.mainFrame().evaluate(contents);
                }

                await injectFile(page, jquery_path);

                await page.waitForTimeout(4000)

                const [chat_button] = await page.$$(".artdeco-card .pvs-profile-actions a.artdeco-button[href*='/messaging/thread/']");
                
                await chat_button.click();


                await page.waitForTimeout(2000)
                await page.keyboard.type(user_mex.message);
                await page.waitForTimeout(2000)
                const [button_send] = await page.$$("button.msg-form__send-button[type='submit']");
                if( button_send ){
                    await button_send.click()
                } else{
                    page.keyboard.press('Enter');    
                }

                const page_result = await page.evaluate(({id_mex}) => {
                    function launch_update_status(){

                        return new Promise(function (resolve) {

                            const ajax_url = 'https://commercharlie.com/scrappup/request';


                            var request_data = {
                                'action': 'inbox_queue_response',
                                'id_mex': id_mex
                            };

                            jQuery.ajax({
                                type: "POST",
                                url: ajax_url,
                                dataType: "JSON",
                                async: false,
                                data: request_data,
                                success: function (resp) {
                                    console.log('ajax success')
                                    resolve('ok')
                                },
                                error: function (request, status, error) {
                                    console.log('ajax error')
                                    resolve(error)
                                }
                            });


                        })
                    }
                    
                    return launch_update_status();
                }, {id_mex});

                console.log(page_result);

            } catch(err) {
                console.log(err);
            }

        }

      }

      await browser.close();

})();
