const puppeteer = require('puppeteer');
const fs = require('fs');

(async () => {
    const browser = await puppeteer.launch({
        ignoreHTTPSErrors: true,
        headless: false,
        devtools: true,
        args: [
            '--no-sandbox',
            '--disable-web-security',
            'http://scraperapi.country_code=de:a0894a612f91c48b5f2f66a2810faf2b@proxy-server.scraperapi.com:8001'
        ]
    });

    const cookie = {
        name: 'li_at',
        value: 'AQEDAQEjVDYEfenJAAABfpYeo_oAAAF-uisn-k0AQ6yMJjHDY3hP93V2yD0-B7iaiZPBXvREvEEbHaG1v8uKzXJOCS6hv_biTYh8ao1oGlmSyGVZFKAfjSm0HnJYlnoYuOpu0lEHRMvCArcfj3OxI6zh',
        domain: '.www.linkedin.com',
        url: 'https://www.linkedin.com',
        path: '/',
        httpOnly: true,
        secure: true
    }

    const page = await browser.newPage();
    const jquery_path = 'node_modules/jquery/dist/jquery.js';

    async function injectFile(page, filePath) {
        let contents = await new Promise((resolve, reject) => {
            fs.readFile(filePath, 'utf8', (err, data) => {
                if (err) return reject(err);
                resolve(data);
            });
        });
        contents += `//# sourceURL=` + filePath.replace(/\n/g, '');
        return page.mainFrame().evaluate(contents);
    }


    // await page.authenticate({username:'3a6a043d31e3440cae816fe3cc6b4b31:',password:''})

    console.log('Opening page ...');
    try {
        const page_url = "https://www.linkedin.com";
        const waitTillHTMLRendered = async (page, timeout = 30000) => {
            const checkDurationMsecs = 1000;
            const maxChecks = timeout / checkDurationMsecs;
            let lastHTMLSize = 0;
            let checkCounts = 1;
            let countStableSizeIterations = 0;
            const minStableSizeIterations = 3;

            while(checkCounts++ <= maxChecks){
                let html = await page.content();
                let currentHTMLSize = html.length;

                // let bodyHTMLSize = await page.evaluate(() => document.body.innerHTML.length);
                // console.log('last: ', lastHTMLSize, ' <> curr: ', currentHTMLSize, " body html size: ", bodyHTMLSize);

                if(lastHTMLSize != 0 && currentHTMLSize == lastHTMLSize)
                    countStableSizeIterations++;
                else
                    countStableSizeIterations = 0; //reset the counter

                if(countStableSizeIterations >= minStableSizeIterations) {
                    // console.log("Page rendered fully..");
                    break;
                }

                lastHTMLSize = currentHTMLSize;
                await page.waitForTimeout(checkDurationMsecs);
            }
        };
        await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36");
        await page.setBypassCSP(true);
        await page.goto(page_url, {waitUntil: "load", timeout: 180000});
        await page.setCookie(cookie)
        await waitTillHTMLRendered(page)
        await injectFile(page, jquery_path);

        await page.evaluate(() => {
            console.log(location.href);
        });
    } catch(err) {
        console.log(err);
    }

    // console.log('Taking a screenshot ...');
    // await page.screenshot({path: 'lease.jpg'});
    // await browser.close();
})();
