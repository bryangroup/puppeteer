const puppeteer = require('puppeteer');
const fs = require('fs');
const util = require('util');
const log_file = fs.createWriteStream(__dirname + '/storage/logs/scrapedin_target.log', {flags: 'w'});

function getArgs () {
    const args = {};
    process.argv
        .slice(2, process.argv.length)
        .forEach( arg => {
            // long arg
            if (arg.slice(0,2) === '--') {
                const longArg = arg.split('=');
                const longArgFlag = longArg[0].slice(2,longArg[0].length);
                const longArgValue = longArg.length > 1 ? longArg[1] : true;
                args[longArgFlag] = longArgValue;
            }
            // flags
            else if (arg[0] === '-') {
                const flags = arg.slice(1,arg.length).split('');
                flags.forEach(flag => {
                    args[flag] = true;
                });
            }
        });
    return args;
}

(async () => {
    var is_debug = false;
    var puppeteer_options = {
        ignoreHTTPSErrors: true,
        headless: false,
        devtools: true,
        args: ["--no-sandbox",  '--disable-web-security']
    };
    var jquery_path = 'node_modules/jquery/dist/jquery.js'
    if(!is_debug) {
        puppeteer_options = {
            ignoreHTTPSErrors: true,
            headless: true,
            devtools: false,
            args: ["--no-sandbox",  '--disable-web-security']
        };
        jquery_path = '/var/www/puppeteer/node_modules/jquery/dist/jquery.js'
    }

    const args = getArgs();
    var authcookie = args.cookie;
    var profile_page = args.page;
    var process_id = process.pid;
    var ext = args.ext;
    var id_company_user = args.idu;
    var id_target = args.target;
    console.log('launched');

    const browser = await puppeteer.launch( puppeteer_options );

    const cookie = {
        name: 'li_at',
        value: authcookie,
        domain: '.www.linkedin.com',
        url: 'https://www.linkedin.com',
        path: '/',
        httpOnly: true,
        secure: true
    }
    const page = await browser.newPage();

    try {
        function node_logger(id_user, id_process, current_page, text, is_error = false) {
            const date_ob = new Date();
            const hours = date_ob.getHours();
            const minutes = date_ob.getMinutes();
            const seconds = date_ob.getSeconds();
            const day = date_ob.getDate();
            const month = date_ob.getMonth();
            const year = date_ob.getFullYear();
            const dt = '['+year+'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds+']';
            const log_line = dt + ' idu ' + id_user + ' - idp ' + id_process + ' - url ' + current_page + ': ' + util.format(text) + ' \n';
            log_file.write( log_line );
        }
        node_logger(ext, process_id, profile_page, 'Launched');

        await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36");
        await page.setCookie(cookie)
        await page.setBypassCSP(true);
        await page.goto(profile_page, { waitUntil: "load", timeout: 180000});

        async function injectFile(page, filePath) {
            let contents = await new Promise((resolve, reject) => {
                fs.readFile(filePath, 'utf8', (err, data) => {
                    if (err) return reject(err);
                    resolve(data);
                });
            });
            contents += `//# sourceURL=` + filePath.replace(/\n/g,'');
            return page.mainFrame().evaluate(contents);
        }

        await injectFile(page, jquery_path);
        node_logger(ext, process_id, profile_page, 'jQuery injected');

        const page_result = await page.evaluate(({ext, profile_page, authcookie, id_company_user, id_target, process_id}) => {
            /** FUNCTIONS - START **/
            function getUrlParameter(sParam) {
                let sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
            }

            function logger(id_user, id_process, current_page, text, is_error = false){
                const log_line = 'idu ' + id_user + ' - idp ' + id_process + ' - url ' + current_page + ': ' + JSON.stringify(text) + ' \n';
                const ajax_data = {
                    'action': 'target_log',
                    'text': log_line,
                    'is_error': is_error,
                };

                $.ajax({
                    type: "POST",
                    url: 'https://scrap.commercharlie.com/scrapedin/request',
                    dataType: "JSON",
                    async: false,
                    data: ajax_data,
                    success: function (resp) {
                    }
                });
            }
            function removeExtraSpaces(string){ return string.replace(/\s{2,}/g, '');}
            /** FUNCTIONS - END **/

            const $ = window.$;
            const ajax_url = 'https://commercharlie.com/scrappup/request';

            function launch_main_interval(){
                return new Promise( function(resolve){
                    var cookie_interval = setInterval(function(){
                        const is_cookie_ok = $('html.theme').length;
                        console.log('Online: '+is_cookie_ok);
                        var cookie_status = 'expired';
                        if(is_cookie_ok > 0)
                            cookie_status = 'ok';

                        logger(ext, process_id, profile_page, 'Cookie status '+ cookie_status);

                        var request_data = {
                            'action': 'target_cookie_status',
                            'id_user': ext,
                            'cookie_val': authcookie,
                            'cookie_status': cookie_status,
                            'id_target': id_target,
                            'id_process': process_id,
                        };

                        $.ajax({
                            type: "POST",
                            url: ajax_url,
                            dataType: "JSON",
                            async: false,
                            data: request_data,
                            success: function (resp) {
                            },
                            error: function (request, status, error) {
                                logger(ext, process_id, current_url,'Cookie status AJAX ERROR - '+request.responseText, true);
                                clearInterval(cookie_interval);
                                resolve('cookie_ajax_error');
                            }
                        });

                        // Il resolve ritorna il comando di stoppare la macchina
                        if(is_cookie_ok <= 0 ){
                            clearInterval(cookie_interval);
                            resolve('cookie_not_ok');
                        }

                        var is_interrupted = false;
                        var is_user_unavailable = false;
                        var current_url = window.location.href;
                        current_url = current_url.replace(',', '%2C');

                        console.log('--- SCRAPER started ---');

                        if (current_url.includes('/signup/'))
                            is_interrupted = true;
                        if (current_url.includes('/authwall'))
                            is_interrupted = true;
                        if (current_url.includes('/challenge/'))
                            is_interrupted = true;
                        if (current_url.includes('/uas/login'))
                            is_interrupted = true;
                        if(current_url.includes('/in/unavailable')) {
                            is_user_unavailable = true;
                            check_double_page++;
                        }

                        if(is_interrupted) {
                            // logger(ext, process_id, current_url,'Interrupted!', true);
                            var request_data = {
                                'action': 'target_cookie_status',
                                'id_user': ext,
                                'cookie_val': authcookie,
                                'cookie_status': 'expired',
                                'id_target': id_target,
                                'id_process': process_id,
                            };

                            $.ajax({
                                type: "POST",
                                url: ajax_url,
                                dataType: "JSON",
                                async: false,
                                data: request_data,
                                success: function (resp) {
                                },
                                error: function (request, status, error) {
                                    logger(ext, process_id, current_url,'Interrupted AJAX ERROR - '+request.responseText, true);
                                    clearInterval(cookie_interval);
                                    resolve('interrupted_ajax_error');
                                }
                            });

                            clearInterval(cookie_interval);
                            resolve('interrupted');
                        }

                        if(!is_interrupted) {
                            /** START PURE SCRAPING **/
                            if (!is_user_unavailable) {
                                $("body,html").animate({scrollTop: $(document).height()}, "slow");

                                if ($('#line-clamp-show-more-button').length) {
                                    $('#line-clamp-show-more-button').click();
                                    $('#line-clamp-show-more-button')[0].click();
                                }
                                if ($('.inline-show-more-text__button').length) {
                                    $('.inline-show-more-text__button').click();
                                    $('.inline-show-more-text__button')[0].click();
                                }

                                var connections = '';
                                var connections_founded = false;
                                if ($('.ph5.pb5 .pv-top-card--list.pv-top-card--list-bullet li.inline-block:nth-child(2)').length) {
                                    connection = $('.ph5.pb5 .pv-top-card--list.pv-top-card--list-bullet li.inline-block:nth-child(2)').text();
                                    connections_founded = true;
                                }
                                if ($('section.pv-top-card .pv-top-card--list-bullet .t-bold').length) {
                                    connection = $('section.pv-top-card .pv-top-card--list-bullet .t-bold').text();
                                    connections_founded = true;
                                }
                                if(!connections_founded){
                                    logger(ext, process_id, current_url,'Connections class not founded!', true);
                                } else {
                                    connections = removeExtraSpaces(connections);
                                    connections = connections.replace(' collegamenti', '');
                                    connections = connections.replace(' collegamento', '');
                                }
                                var role_all = '';
                                var role = '';
                                var role_founded = false;
                                if ($('.ph5.pb5 h2.t-black.t-normal.t-18').length) {
                                    role_all = $('.ph5.pb5 h2.t-black.t-normal.t-18').text();
                                    role_founded = true;
                                }
                                if ($('section.pv-top-card .text-body-medium').length){
                                    role_all = $('section.pv-top-card .text-body-medium').text();
                                    role_founded = true;
                                }

                                if(!role_founded){
                                    logger(ext, process_id, current_url,'Role class not founded!', true);
                                } else {
                                    role_all = removeExtraSpaces(role_all);
                                    var split_role = role_all.split(' presso ');
                                    if (split_role.length == 1)
                                        split_role = role_all.split('@');
                                    if (split_role.length == 1)
                                        split_role = role_all.split(' at ');
                                    if (split_role.length == 1)
                                        split_role = role_all.split(', ');
                                    role = split_role[0];
                                }

                                var connections_open = $('a[data-control-name=topcard_view_all_connections]').length;
                                var informations = '';

                                if ($('.pv-about-section .pv-about__summary-text .lt-line-clamp__line').length) {
                                    $('.pv-about-section .pv-about__summary-text .lt-line-clamp__line').each(function () {
                                        informations = informations + ' ' + removeExtraSpaces($(this).text());
                                    });
                                }
                                if ($('.pv-about-section .inline-show-more-text').length) {
                                    informations = $('.pv-about-section .inline-show-more-text').text();
                                }
                                var user_url = location.href;
                                var fullname = firstname = lastname = '';
                                var name_founded = false;
                                if ($('.pv-top-card .t-24.t-black.t-normal').length){
                                    fullname = $('.pv-top-card .t-24.t-black.t-normal').text();
                                    name_founded = true;
                                }
                                if ($('section.pv-top-card h1.text-heading-xlarge').length) {
                                    fullname = $('section.pv-top-card h1.text-heading-xlarge').text();
                                    name_founded = true;
                                }

                                if(!name_founded){
                                    logger(ext, process_id, current_url,'Name class not founded!', true);
                                } else {
                                    fullname = removeExtraSpaces(fullname);
                                    firstname = fullname.split(' ').slice(0, -1).join(' ');
                                    lastname = fullname.split(' ').slice(-1).join(' ');
                                }

                                var user_location = '';
                                var user_location_founded = false;
                                if($('.pv-top-card .pv-text-details__left-panel .text-body-small.inline').length){
                                    user_location = removeExtraSpaces( $('.pv-top-card .pv-text-details__left-panel .text-body-small.inline').text() );
                                    user_location_founded = true;
                                }

                                if(!user_location_founded)
                                    logger(ext, process_id, current_url,'User location class not founded!', true);

                                    var photo = '';
                                var photo_founded = false;
                                if($('.pv-top-card .pv-top-card__photo img').length){
                                    photo = $('.pv-top-card .pv-top-card__photo img').attr('src');
                                    photo_founded = true;
                                }

                                if(!photo_founded)
                                    logger(ext, process_id, current_url,'Photo class not founded!', true);
                                    // TODO CHECK Computed Summary

                                    var request_data = {
                                        'action': 'target_update',
                                        'id_user': ext,
                                        'firstname': firstname,
                                        'lastname': lastname,
                                        'role': role,
                                        'location': user_location,
                                        'photo': photo,
                                        'page': user_url,
                                        'connections_open': connections_open,
                                        'connections_count': connections,
                                        'id_company_user': id_company_user,
                                        'id_target': id_target,
                                    };

                                logger(ext, process_id, current_url,'Target data sent');
                                logger(ext, process_id, current_url, request_data);

                                $.ajax({
                                    type: "POST",
                                    url: ajax_url,
                                    dataType: "JSON",
                                    async: false,
                                    data: request_data,
                                    success: function (resp) {
                                        logger(ext, process_id, current_url,'Target data response (success)');
                                        logger(ext, process_id, current_url, resp);
                                    },
                                    error: function (request, status, error) {
                                        logger(ext, process_id, current_url,'Target data AJAX ERROR - '+request.responseText, true);
                                        clearInterval(cookie_interval);
                                        resolve('target_data_ajax_error');
                                    }
                                });
                                clearInterval(cookie_interval);
                                resolve('ok');
                                /** END PURE SCRAPING **/
                            }
                        }
                    }, 6000);
                } );

            }

            return launch_main_interval().then(function(x){ return x; });
        }, {ext, profile_page, authcookie, id_company_user, id_target, process_id});

        // Comando ritornato dal promise resolve
        console.log(page_result);
        node_logger(ext, process_id, profile_page, 'Finish with page result: '+page_result);
        await browser.close();
    } catch(err) {
        console.log(err);
        node_logger(ext, process_id, profile_page, err);
        await browser.close();
    }
})();
